//
//  places.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 27/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

struct placesUser : Decodable {
    let status: String
    let message: String
    let data: placesUserData
}

struct placesUserData : Decodable {
    
    let user_id: String?
    let first_name: String?
    let last_name: String?
    let other_name: String?
    let fullname: String?
    let alias: String?
    let about: String?
    let sex_id: String?
    let sex: String?
    let birth_date: String?
    let relation_status_id: String?
    let relation_status: String?
    let relation_user_id: String?
    let relation_user_first_name: String?
    let relation_user_last_name: String?
    let relation_user_fullname: String?
    let location: String?
//    let contact: String?
    let profile_pic: String?
    let profile_folder_name: String?
    let profile_pic_filename: String?
    let cover_pic: String?
    let cover_folder_name: String?
    let cover_pic_filename: String?
    let request_counter: String?
    let dream_destination_list: Int?
    //let hidden_content_list: Int?
    let email: String?
    let username: String?
    let password: String?
    let salt: String?
    let social_media_auth: String?
    let social_media_id: Int?
    let date_created: String?
    let last_seen: String?
    let is_suspended: String?
    let is_notified: String?
    let approve_token: String?
    let device_token: String?
    let language_id: String?
    let language_machine_name: String?
    let language_label: String?
    let active: String?
    let places: [placesData]?
}

struct placesData : Decodable {
    let place_id: String?
    let name: String?
    let username: String?
    let place_type_id: String?
    let place_type_label: String?
    let place_category_id: String?
    let place_category_label: String?
    let label: String?
    let color_scheme: String?
    let address: String?
    let latitude: String?
    let longitude: String?
    let geoname_id: String?
    let about: String?
    let contact: placeContact?
    let open_hours_list: String?
    let profile_pic: String?
    let profile_pic_filename: String?
    let cover_pic_filename: String?
    let cover_folder_name: String?
    let feature_list: String?
    let like_list: [likeData]?
    let visited_list: String?
    let date_created: String?
    let is_flagged: String?
    let flag_history: String?
    let is_official: String?
    let is_published: String?
    let is_viewable: String?
    let active: String?
    let rating: ratingsData?
}
struct placeContact : Decodable {
    let email : [String]?
    let phone : [String]?
    let website : [String]?
}
struct ratingsData : Decodable {
    let average_score: Int?
    let reviewers : Int?
    
}
struct likeData : Decodable {
    let user_id: String?
    let name: String?
    let date_time: String?
}
