//
//  LocationTableViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 25/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class LocationTableViewController: UITableViewController, UISearchBarDelegate  {

    @IBOutlet weak var searchBar: UISearchBar!
    
    var postActivity:UIActivityIndicatorView = UIActivityIndicatorView()
    
    var destinationMartha  : String?
    var locationArray = [location]()
    var newLocArray = [String]()
    var testArray = [String:[String]]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
   
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    //DO something when searbar cancel button is clicked
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.view.endEditing(true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        startAnimating()
        
        searchBar.setShowsCancelButton(true, animated: true)
        
        print("Search Pressed!~")
        
        let keywords = searchBar.text
        let finalKeywords = keywords?.replacingOccurrences(of: " ", with: "+")
        
        guard let url = URL(string: "https://us1.locationiq.org/v1/search.php?key=94b144627b4d5b&q=\(finalKeywords!)&format=json")else{return}
        
        //request url
        var request = URLRequest(url: url as URL)
        
        // method to pass data
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let locationInfo = try JSONDecoder().decode([location].self, from: data)

                print(locationInfo)
                self.locationArray = locationInfo

                for hotelsArray in self.locationArray{
                    self.stopAnimating()
                    print(hotelsArray)
                    DispatchQueue.main.async(execute: {
                        print("tableReloaded")

                        self.tableView.reloadData()
                    })

                }



             
                
            }catch{
                self.stopAnimating()
                print(error)
                
            }
            
        }
        DispatchQueue.main.async(execute: {
            self.stopAnimating()
            print("tableReloaded")
            self.tableView.reloadData()
            self.view.endEditing(true)
        })
        
        task.resume()
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
          self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return locationArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "placeCell", for: indexPath) as! LocationTableViewCell
        
        print(locationArray[indexPath.row].display_name!)
        
        cell.locationLabel.text = locationArray[indexPath.row].display_name
        
        // Configure the cell...

        return cell
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? hotelsViewController {
            
//            destination.postDetails = postsArray[(tableView.indexPathForSelectedRow?.row)!]
            
            destination.locationDetails = locationArray[(tableView.indexPathForSelectedRow?.row)!]
            
            
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func startAnimating() {
        postActivity.center = self.view.center
        
        postActivity.hidesWhenStopped = true
        postActivity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(postActivity)
        
        postActivity.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        postActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
    
    
}
