//
//  NotificationsTableViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 28/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class NotificationsTableViewController: UITableViewController {

    var searchController: UISearchController!
    
    var notificationsArray = [notificationData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // make profile pic round circle
        //notificationProfilePic.layer.cornerRadius = notificationProfilePic.frame.size.width / 2
       //notificationProfilePic.clipsToBounds = true
        
        
        //fetch places
        DispatchQueue.main.async(execute: {
            self.loadNotification()
        })
        
        //self.setupSearchController()
        tableView.separatorStyle = .none
        
        // dynamic table view cell height
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func setupSearchController()
    {
        searchController = UISearchController(searchResultsController: nil)
        
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = true
        searchController.searchBar.barStyle = .black
        
        navigationItem.titleView = searchController.searchBar
        definesPresentationContext = true
    }
    
    //function to fetch posts from the internet and load them!
    func loadNotification(){
        userID = UserDefaults.standard.object(forKey: "userId") as? String
        
        let userNum = userID!
        guard let url = URL(string: "https://deiplaces.com/api/index.php/user/user_notifications?user_id=\(userNum)")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let notifications = try JSONDecoder().decode(notificationPost.self, from: data)
                
                if notifications.data.isEmpty{
                    print("You don't have any notifications")
                    
                }else{
                    self.notificationsArray = notifications.data
                    //iterate through array to get specific values from struct
                    
                    for notificationArray in self.notificationsArray{
                        //print(postArray.content)
                        print(notificationArray)
                        DispatchQueue.main.async(execute: {
                            self.tableView.reloadData()
                        })
                        
                    }
                    
                    
                    
                }
              
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return notificationsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationCell
     
        cell.notificationName.text = notificationsArray[indexPath.row].first_name1
        cell.notificationLabel.text = notificationsArray[indexPath.row].str_format
        
        //load post pic from server library
        var notificationImageName : String?
        
        if notificationsArray[indexPath.row].profile_pic_filename1 != nil {
            notificationImageName = notificationsArray[indexPath.row].profile_pic_filename1
            
            let urlNotificationImageString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(notificationImageName)!
            
            let notificationsImageUrl = URL(string: urlNotificationImageString)
            
            //cell.placesProfilePic.downloadedFrom(url: notificationsImageUrl!)
            cell.notificationProfilePic.downloadedFrom(url: notificationsImageUrl!)
        }else{
            print("Place has no picture")
            
        }
        
        
        // Configure the cell...

        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
