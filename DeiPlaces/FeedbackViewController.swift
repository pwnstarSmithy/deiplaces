//
//  FeedbackViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 26/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {

    
    //@IBOutlet weak var feedbackText: UITextView!
    
    @IBOutlet weak var feedbackText: UITextView!
    @IBAction func sendFeedback(_ sender: Any) {
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        let finalFrontier = feedbackText.text
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/user/create-ticket")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "user_id=\(userId)&message=\(finalFrontier!)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        //let displayMessage = parseJSON["data"]
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                          let alertController = UIAlertController(title: "Feedback received", message: "We will get back you shortly!", preferredStyle: .alert)
                            
                            let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
                            
                            alertController.addAction(okAction)
                            
                            self.present(alertController, animated: true, completion: nil )
                        }else{
                            
                            
                           
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                            
                            print(error)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
