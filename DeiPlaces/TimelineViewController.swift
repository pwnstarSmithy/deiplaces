//
//  TimelineViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 01/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class TimelineViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var timelineCoverPic: UIImageView!
    
    @IBOutlet weak var timelineProfilePictureTop: UIImageView!
    
    @IBOutlet weak var timelineUsernameTop: UILabel!
    
    var postActivity:UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var tableView: UITableView!
    
    var photo_type : String?
    let targetType = "user"
    
    var uuid = String()
    var imageSelected = false
    
    var userInfo : userData?
    
    var profilePicture : String?
    var coverPicture : String?
    
    
    @IBAction func changeCover(_ sender: Any) {
        
        
        photo_type = "cover"
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Take a Pic", style: .default, handler: { action in
            //call function to load camera
            self.takePic()
            
            print("Take a pic")
        }))
        
        alertController.addAction(UIAlertAction(title: "Choose from photo library", style: .default, handler: { action in
            //call function to load photo library
            self.chooseLibrary()
            print("Choose from library")
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        
        self.present(alertController, animated: true, completion: nil )
        
        
    }
    

    @IBAction func changeProfile(_ sender: Any) {
        
        photo_type = "profile"
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Take a Pic", style: .default, handler: { action in
            //call function to load camera
            self.takePic()
            print("Take a pic")
        }))
        
        alertController.addAction(UIAlertAction(title: "Choose from photo library", style: .default, handler: { action in
            //call function to load photo library
            self.chooseLibrary()
            print("Choose from library")
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        
        self.present(alertController, animated: true, completion: nil )
        
        
        
    }
    
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if photo_type == "cover"{
            timelineCoverPic.image = info[UIImagePickerControllerEditedImage] as? UIImage
            self.dismiss(animated: true, completion: nil)
            
            //cast as true to save image file to server
            imageSelected = true
            
            //upload the pic to the server
            uploadPic()
            
        }else{
            timelineProfilePictureTop.image = info[UIImagePickerControllerEditedImage] as? UIImage
            self.dismiss(animated: true, completion: nil)
            
            //cast as true to save image file to server
            imageSelected = true
            
            //upload the pic to the server
            uploadPic()
        }
        
        
    }
    
    // custom body of HTTP request to upload image file
    @objc func createBodyWithParams(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        
        // if file is not selected, it will not upload a file to server, because we did not declare a name file
        var filename = ""
        
        if imageSelected == true {
            filename = "post-\(uuid).jpg"
        }
        
        
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")
        
        body.appendString("--\(boundary)--\r\n")
        
        return body as Data
        
    }
    //function to choose from photolibrary
    func chooseLibrary(){
        
        //calling picker for selecting image
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
        
        
        
    }
    
    func takePic(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    
    func uploadPic(){
        startAnimating()
        userID = UserDefaults.standard.object(forKey: "userId") as? String
        
        let userNum = userID!
        
        print("Upload Pic initiated!")
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/photos/profile-cover-photo-upload")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        // param to be passed to php file
        let param = [
            
            "photo_type" : photo_type!,
            "target_type" : targetType,
            "target_id" : userId,
            "creator_id" : userNum
        ]
        
        
        // body
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        // if picture is selected, compress it by half
        var imageData = Data()
        
        if photo_type == "cover" {
            
            if timelineCoverPic.image != nil {
                imageData = UIImageJPEGRepresentation(timelineCoverPic.image!, 0.5)!
            }
            
        }else {
            
            if timelineProfilePictureTop.image != nil {
                imageData = UIImageJPEGRepresentation(timelineProfilePictureTop.image!, 0.5)!
            }
            
        }
        
        // Continue Body... body
        request.httpBody = createBodyWithParams(param, filePathKey: "file", imageDataKey: imageData, boundary: boundary)
        
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        self.stopAnimating()
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        print(message!)
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            // reset UI
                            self.loadUser()
                            self.imageSelected = false
                            
                        }
                        
                    } catch {
                        self.stopAnimating()
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    self.stopAnimating()
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
        
    }
    
    
    var postsArray = [postData]()
//    var userInfo : userData?
    var comments : Int?
    var likes : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        //fetch posts!
        DispatchQueue.main.async(execute: {
            self.loadPosts()
            self.loadUser()
        })
        
        tableView.separatorStyle = .none
        
        // Do any additional setup after loading the view.
        
      
        //make profile picture edges round.
        timelineProfilePictureTop.layer.cornerRadius = timelineProfilePictureTop.bounds.width / 20
        timelineProfilePictureTop.clipsToBounds = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadUser(){
        
        //
        userID = UserDefaults.standard.object(forKey: "userId") as? String
        
        let userNum = userID!
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/user/single-user?user_id=\(userNum)")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userInfor = try JSONDecoder().decode(user.self, from: data)
                
                //print(userPosts.data)
                //print("it works")
                
                self.userInfo = userInfor.data
                DispatchQueue.main.async(execute: {
                    let coverPicFile = self.userInfo?.cover_pic_filename
                    
                    if coverPicFile != nil {
                        //load profile picture from library
                        let urlString = "https://deiplaces.com/uploads/cover-picture/image_800/"+(coverPicFile)!
                        let profileURL = URL(string: urlString)
                        
                        self.coverPicture = coverPicFile
                        
                        self.timelineCoverPic?.downloadedFrom(url: profileURL!)
                        
                    }else{
                        print("You don't have a profile pic!")
                    }
                    
                    let profilePicFile = self.userInfo?.profile_pic_filename
                    
                    if profilePicFile != nil {
                        
                        //load profile picture from library
                        let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(profilePicFile)!
                        let profileURL = URL(string: urlString)
                        
                        self.profilePicture = profilePicFile
                        
                        self.timelineProfilePictureTop?.downloadedFrom(url: profileURL!)
                        
                    }else{
                        print("You have no profile picture!")
                    }
                })
                
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
    }
    
    
    //function to fetch posts from the internet and load them!
    func loadPosts(){
        startAnimating()
        userID = UserDefaults.standard.object(forKey: "userId") as? String
        
        let userNum = userID!
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/posts/target_posts?target_type_id=1&target_id=\(userNum)&start=1&rows=10")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userPosts = try JSONDecoder().decode(deiPost.self, from: data)
                
                //print(userPosts.data)
                //print("it works")
                
                self.postsArray = userPosts.data
                //iterate through array to get specific values from struct
                self.stopAnimating()
                for postArray in self.postsArray{
                    //print(postArray.content)
                    print(postArray)
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                    
                    
                }
                
            }catch{
                self.stopAnimating()
                print(error)
                
            }
            
        }
        task.resume()
        
    }
    
    
     public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if section == 0{
            return 1
        }else{
            return postsArray.count
        }
        
    }
    

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
         if indexPath.section == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "statusCellTimeline", for: indexPath) as! TimelineStatusTableViewCell
        
     
        if profilePicture != nil {
            //load profile picture from library
            let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(profilePicture)!
            let profileURL = URL(string: urlString)
            
            cell.timelineProfilePic?.downloadedFrom(url: profileURL!)
            //timelineProfilePictureTop?.downloadedFrom(url: profileURL!)
            
        }else{
            
            print("you have no profile picture set")
            
        }
             return cell
         }else{
            
            if postsArray[indexPath.row].photos != nil {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PostCellTimeline", for: indexPath) as! TimelinePostsTableViewCell
                
                
                if postsArray[indexPath.row].comments != nil {
                    
                    comments = postsArray[indexPath.row].comments?.count
                }else {
                    
                    comments = 0
                }
                
                if postsArray[indexPath.row].like_list != nil {
                    
                    likes = postsArray[indexPath.row].like_list?.count
                    
                }else{
                    
                    likes = 0
                }
                
                cell.timelineUsername.text = postsArray[indexPath.row].fullname
                timelineUsernameTop.text = postsArray[indexPath.row].fullname
                cell.timelineHours.text = postsArray[indexPath.row].data_created
                cell.timelineCaption.text = postsArray[indexPath.row].content
                //cell.timeAgoLabel.text = postsArray[indexPath.row].modified
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(postsArray[indexPath.row].profile_pic_filename)!
                let profileURL = URL(string: urlString)
                
                cell.timelinePostsProfPic.downloadedFrom(url: profileURL!)
                
                //15 Likes     30 Comments     500 Shares
                cell.timelineStats.text = "\(likes!) Likes      \(comments!)  Comments"
                
                //iterate through posts images images array
                
                //load post picture from server library
                var postImageName : String?
                if postsArray[indexPath.row].photos != nil{
                    let postImage = postsArray[indexPath.row].photos
                    for postsImage in postImage!{
                        
                        postImageName = postsImage.filename!
                        
                    }
                    let urlPostImageString = "https://deiplaces.com/uploads/post-picture/"+(postImageName)!
                    let postsImageUrl = URL(string: urlPostImageString)
                    cell.timelinePicture.downloadedFrom(url: postsImageUrl!)
                }else{
                    print("Post has no picture")
                }
                //return cell
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoImagePostCellTimeline", for: indexPath) as! NoImageTimelinePostsTableViewCell
                if postsArray[indexPath.row].comments != nil {
                    
                    comments = postsArray[indexPath.row].comments?.count
                }else {
                    
                    comments = 0
                }
                
                if postsArray[indexPath.row].like_list != nil {
                    
                    likes = postsArray[indexPath.row].like_list?.count
                    
                }else{
                    
                    likes = 0
                }
                
                cell.noImageUsername.text = postsArray[indexPath.row].fullname
                cell.noimageTime.text = postsArray[indexPath.row].data_created
                cell.noImageCaption.text = postsArray[indexPath.row].content
                
                cell.noImageStats.text = "\(likes!) Likes      \(comments!)  Comments"
                
                if postsArray[indexPath.row].profile_pic_filename != nil{
                    //load profile picture from library
                    let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(postsArray[indexPath.row].profile_pic_filename)!
                    let profileURL = URL(string: urlString)
                    
                    cell.noImageProfPic.downloadedFrom(url: profileURL!)
                }else {
                    print("User has no profile pic")
                }
                
                
                return cell
            }
            
     
    }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func startAnimating() {
        postActivity.center = self.view.center
        
        postActivity.hidesWhenStopped = true
        postActivity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(postActivity)
        
        postActivity.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        postActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }

}
