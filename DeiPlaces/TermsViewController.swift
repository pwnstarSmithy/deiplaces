//
//  TermsViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 25/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController {

    @IBAction func termsPressed(_ sender: Any) {
        if let url = NSURL(string: "https://deiplaces.com/index.php/info/info/privacy") {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
        
    }
    
    @IBAction func privacyPressed(_ sender: Any) {
        
        
        
    }
    
    @IBAction func registerPressed(_ sender: Any) {
        
        let registerPageView =  self.storyboard?.instantiateViewController(withIdentifier: "Register")
        self.present(registerPageView!, animated: true, completion: nil)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
