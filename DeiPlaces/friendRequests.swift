//
//  friendRequests.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 25/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
struct requestList : Decodable {
    let status: String
    let message: String
    let data: requestListData
}

struct requestListData : Decodable {
    
    let user_id: String?
    let first_name: String?
    let last_name: String?
    let other_name: String?
    let fullname: String?
    let alias: String?
    let about: String?
    let sex_id: String?
    let sex: String?
    let birth_date: String?
    let relation_status_id: Int?
    let relation_status: String?
    let relation_user_id: Int?
    let relation_user_first_name: String?
    let relation_user_last_name: String?
    let relation_user_fullname: String?
    let location: String?
    let profile_pic: String?
    let profile_folder_name: String?
    let profile_pic_filename: String?
    let cover_pic: String?
    let cover_folder_name: String?
    let cover_pic_filename: String?
    let request_counter: String?
    let dream_destination_list: Int?
    //let hidden_content_list: Int?
    let email: String?
    let username: String?
    let password: String?
    let salt: String?
    let social_media_auth: String?
    let social_media_id: Int?
    let date_created: String?
    let last_seen: String?
    let is_suspended: String?
    let is_notified: String?
    let approve_token: String?
    let device_token: String?
    let language_machine_name: String?
    let language_label: String?
    let active: String?
    let followers: [requestData]?
}
struct requestData : Decodable {
    let user_id: String?
    let first_name: String?
    let last_name: String?
    let other_name: String?
    let fullname: String?
    let alias: String?
    let about: String?
    let sex_id: String?
    let sex: String?
    let birth_date: String?
    let relation_status_id: Int?
    let relation_status: String?
    let relation_user_id: Int?
    let relation_user_first_name: String?
    let relation_user_last_name: String?
    let relation_user_fullname: String?
    let location: String?
    let profile_pic: String?
    let profile_folder_name: String?
    let profile_pic_filename: String?
    let cover_pic: String?
    let cover_folder_name: String?
    let cover_pic_filename: String?
    let request_counter: String?
    let dream_destination_list: Int?
    //let hidden_content_list: Int?
    let email: String?
    let username: String?
    let password: String?
    let salt: String?
    let social_media_auth: String?
    let social_media_id: Int?
    let date_created: String?
    let last_seen: String?
    let is_suspended: String?
    let is_notified: String?
    let approve_token: String?
    let device_token: String?
    let language_machine_name: String?
    let language_label: String?
    let active: String?
}

