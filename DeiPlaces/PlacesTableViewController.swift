//
//  PlacesTableViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 27/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class PlacesTableViewController: UITableViewController, placesCellDelegate {

    var reportId : String?
    var postIdentification : String?
    
    var postActivity:UIActivityIndicatorView = UIActivityIndicatorView()

    var searchController: UISearchController!
    var placesArray = [placesData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //fetch places
        DispatchQueue.main.async(execute: {
            self.loadPlaces()
        })
        
        //self.setupSearchController()
        tableView.separatorStyle = .none
        
        // dynamic table view cell height
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewDidAppear(_ animated: Bool) {
        //fetch places
        DispatchQueue.main.async(execute: {
            self.loadPlaces()
        })
    }
    
    func setupSearchController()
    {
        searchController = UISearchController(searchResultsController: nil)
        
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = true
        searchController.searchBar.barStyle = .black
        
        navigationItem.titleView = searchController.searchBar
        definesPresentationContext = true
    }
    //function to fetch posts from the internet and load them!
    func loadPlaces(){
        startAnimating()
        userID = UserDefaults.standard.object(forKey: "userId") as? String
        
        let userNum = userID!
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/user/single-user-places?user_id=\(userNum)")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userPlaces = try JSONDecoder().decode(placesUser.self, from: data)
                
                //print(userPosts.data)
                //print("it works")
                let usersPlaces = userPlaces.data
                
                if usersPlaces.places != nil {
                    self.placesArray = usersPlaces.places!
                    
                    //iterate through array to get specific values from struct
                    self.stopAnimating()
                    for placeArray in self.placesArray{
                        //print(postArray.content)
                        print(placeArray)
                        DispatchQueue.main.async(execute: {
                            self.tableView.reloadData()
                        })
                        
                        
                    }
                }else{
                    self.stopAnimating()
                    print("You haven't added any places")
                }
                
            }catch{
                self.stopAnimating()
                print(error)
                
            }
            
        }
        task.resume()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return placesArray.count
        }
        
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddPlaceCell", for: indexPath) as! AddEventsCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "placesCell", for: indexPath) as! placesCell
            
            cell.placesTitle.text = placesArray[indexPath.row].name
            cell.placesCaption.text = placesArray[indexPath.row].about
            
            
            //cell.placeRating.text = placesArray[indexPath.row].rating
            //cell.placesLikes.text = placesArray[indexPath.row].like_list?.count
            
            //load post pic from server library
            var placeImageName : String?
            
            if placesArray[indexPath.row].profile_pic_filename != nil {
                placeImageName = placesArray[indexPath.row].profile_pic_filename
                
                let urlPlaceImageString = "https://deiplaces.com/uploads/profile-picture/image_500/"+(placeImageName)!
                
                let placesImageUrl = URL(string: urlPlaceImageString)
                
                cell.placesProfilePic.downloadedFrom(url: placesImageUrl!)
                
                cell.placesProfilePic?.layer.cornerRadius = 8.0
                cell.placesProfilePic?.clipsToBounds = true
                
            }else{
                print("Place has no picture")
                
            }
            
            
            // Configure the cell...
            cell.delegate = self
            return cell
        }
    }
    //toPlaceDetail
    //navigation to next view
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPlaceDetail" {
            
            if let indexPath = tableView.indexPathForSelectedRow {
                
                let destinationView = segue.destination as! PlaceDetailsViewController
                
                //destinationView.paymentsIndex = billsArray[indexPath.row].index
                if placesArray[indexPath.row].cover_pic_filename != nil {
                destinationView.placeCoverPicture = placesArray[indexPath.row].cover_pic_filename
                }else{
                    print("not cover pic")
                }
                if placesArray[indexPath.row].profile_pic_filename != nil{
                destinationView.placeProfilePicture = placesArray[indexPath.row].profile_pic_filename
                }else{
                    print("no profile picture")
                }
                if placesArray[indexPath.row].about != nil {
                destinationView.placeAbout = placesArray[indexPath.row].about
                    }else{
                    print("no about content")
                }
                
                if placesArray[indexPath.row].address != nil{
                destinationView.placeAddress = placesArray[indexPath.row].address
                }else{
                    print("no address")
                }
                
                if placesArray[indexPath.row].name != nil {
                destinationView.placeName = placesArray[indexPath.row].name
                }else{
                    print("No name")
                }
                
                if placesArray[indexPath.row].place_category_label != nil {
                    destinationView.placeCategory = placesArray[indexPath.row].place_category_label
                }else{
                    print("No Category")
                }
                
                if placesArray[indexPath.row].place_id != nil {
                    destinationView.placeId = placesArray[indexPath.row].place_id
                }else{
                    print("No ID!")
                }
                
                if placesArray[indexPath.row].contact != nil {
                    
                   let contactDic =  placesArray[indexPath.row].contact
                   
                   let email = contactDic?.email
                    
                    for emails in email! {
                        destinationView.placeEmail.append(emails)
                    }
                    
                   let phone = contactDic?.phone
                    
                    for phones in phone! {
                        destinationView.placePhone.append(phones)
                    }
                    
                }
                
            }
            
        }
        
    }

    func didTapMore(id: String){
        
        postIdentification = id
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        let reportAction = UIAlertAction(title: "Report this place", style: .default, handler: self.sendHandlerNoImage)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
 
        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil )
        
    }
    
    func sendHandler(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Report this place to DeiPlaces", message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "It's Spam", style: .default, handler: { action in
            self.reportId = "1"
        }))
        
        alertController.addAction(UIAlertAction(title: "Nudity", style: .default, handler: { action in
            self.reportId = "2"
        }))
        
        alertController.addAction(UIAlertAction(title: "Violence", style: .default, handler: { action in
            self.reportId = "3"
        }))
        
        alertController.addAction(UIAlertAction(title: " Harassment", style: .default, handler: { action in
            self.reportId = "5"
        }))
        
        alertController.addAction(UIAlertAction(title: "False News", style: .default, handler: { action in
            self.reportId = "5"
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
        }))
        
        self.present(alertController, animated: true, completion: nil )
    }
    func didTapMoreNoImage(id: String){
        
        postIdentification = id
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
   
        let reportAction = UIAlertAction(title: "Report this place", style: .default, handler: self.sendHandlerNoImage)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)

        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil )
        
    }
    
    func sendHandlerNoImage(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Report this place to DeiPlaces", message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "It's Spam", style: .default, handler: { action in
            self.reportId = "1"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Nudity", style: .default, handler: { action in
            self.reportId = "2"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Violence", style: .default, handler: { action in
            self.reportId = "3"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: " Harassment", style: .default, handler: { action in
            self.reportId = "4"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "False News", style: .default, handler: { action in
            self.reportId = "5"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
            self.sendReport()
        }))
        
        self.present(alertController, animated: true, completion: nil )
    }
    
    func thanksNoImage(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Thanks for your feedback", message: "We have a team ready to review this report and act accordingly", preferredStyle: .alert)
        
        
        let thanksAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alertController.addAction(thanksAction)
        
        self.present(alertController, animated: true, completion: nil )
        
    }
    
    
    func sendReport(){
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        let target_type_id = "2"
        let target_id = postIdentification
        let creator_id = userId
        let reasonID  = reportId
        let messageString = ""
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/report-content")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "target_type_id=\(target_type_id)&target_id=\(target_id!)&creator_id=\(creator_id)&reason_id=\(reasonID!)&message=\(messageString)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        let displayMessage = parseJSON["data"]
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            let messageCustom = displayMessage
                            appDelegate.infoView(message: messageCustom as! String, color: colorBrandBlue)
                            
                        }else{
                            
                            self.tableView.reloadData()
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                            
                            print(error)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
        
        
    }
    
    func startAnimating() {
        postActivity.center = self.view.center
        
        postActivity.hidesWhenStopped = true
        postActivity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(postActivity)
        
        postActivity.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        postActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
    
    
}

 
