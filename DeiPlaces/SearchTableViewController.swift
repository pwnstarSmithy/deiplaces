//
//  SearchTableViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 24/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class SearchTableViewController: UITableViewController, UISearchBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    
    var userResultsArray = [userResults]()
    var placeResultsArray = [placeResults]()
    var postResultsArray = [postResults]()
    var eventResultsArray = [eventResults]()
    var searchResult : String?
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    //DO something when searbar cancel button is clicked
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        self.view.endEditing(true)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
       
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        //reset Arrays before making new search
        userResultsArray.removeAll()
        placeResultsArray.removeAll()
        postResultsArray.removeAll()
        eventResultsArray.removeAll()
        
        print("Search Pressed!~")
        
        let keywords = searchBar.text
        let finalKeywords = keywords?.replacingOccurrences(of: " ", with: "+")
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/search/search?searchterm=\(finalKeywords!)")else{return}
        
        //request url
        var request = URLRequest(url: url as URL)
        
        // method to pass data
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let searched = try JSONDecoder().decode(search.self, from: data)
                
                let searchArray = searched.data
                
                let userArray = searchArray.user_results!
                
                for usersArray in userArray {
                    DispatchQueue.main.async(execute: {
                        print(usersArray)
                        self.userResultsArray.append(usersArray)
                        self.tableView.reloadData()
                    })
                }
                //                print(self.userResultsArray)
                
                let placeArray = searchArray.place_results!
                
                for placesArray in placeArray {
                    DispatchQueue.main.async(execute: {
                        
                        self.placeResultsArray.append(placesArray)
                        
                        self.tableView.reloadData()
                    })
                    
                }
                
                //                print(self.placeResultsArray)
                
                
                let postArray = searchArray.post_results!
                
                for postsArray in postArray {
                    DispatchQueue.main.async(execute: {
                        
                        self.postResultsArray.append(postsArray)
                        
                        self.tableView.reloadData()
                    })
                    
                }
                
                //                print(self.postResultsArray)
                
                let eventArray = searchArray.event_results!
                
                for eventsArray in eventArray{
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.eventResultsArray.append(eventsArray)
                        
                        self.tableView.reloadData()
                    })
                    
                }
                
            }catch{
                print(error)
                
            }
            
        }
        DispatchQueue.main.async(execute: {
            print("tableReloaded")
//            self.tableView.reloadData()
            self.view.endEditing(true)
        })

        task.resume()
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return eventResultsArray.count
        }else if section == 1 {
            return userResultsArray.count
        }else if section == 2 {
            return postResultsArray.count
        }else {
            return placeResultsArray.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "eventsCell", for: indexPath) as! SearchEventsTableViewCell
            
            if eventResultsArray[indexPath.row].profile_pic_filename != nil{
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(eventResultsArray[indexPath.row].profile_pic_filename)!
                let profileURL = URL(string: urlString)
                
                cell.eventsImage.downloadedFrom(url: profileURL!)
            }else {
                print("Event has no profile pic")
            }
            
            cell.eventsLabel.text = eventResultsArray[indexPath.row].name
  
            return cell
            
        }else if indexPath.section == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "usersCell", for: indexPath) as! SearchUsersTableViewCell
       
            cell.userLabel.text = userResultsArray[indexPath.row].fullname
            
            if userResultsArray[indexPath.row].profile_pic_filename != nil{
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(userResultsArray[indexPath.row].profile_pic_filename)!
                let profileURL = URL(string: urlString)

                cell.userImage.downloadedFrom(url: profileURL!)
            }else {

            }
            
            
            
            return cell
            
        }else if indexPath.section == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "postsCell", for: indexPath) as! SearchPostsTableViewCell
            
            if postResultsArray[indexPath.row].profile_pic_filename != nil{
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(postResultsArray[indexPath.row].profile_pic_filename)!
                let profileURL = URL(string: urlString)
                
                cell.postsImage.downloadedFrom(url: profileURL!)
            }else {
                print("Post has no profile pic")
            }
            
            
            
            cell.postsLabel.text = postResultsArray[indexPath.row].content
            
       
            
            return cell
            
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "placesCell", for: indexPath) as! SearchPlacesTableViewCell
            
            if placeResultsArray[indexPath.row].profile_pic_filename != nil{
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(placeResultsArray[indexPath.row].profile_pic_filename)!
                let profileURL = URL(string: urlString)
                
                cell.placesImage.downloadedFrom(url: profileURL!)
            }else {
                print("User has no profile pic")
            }
            
            
            cell.searchLabel.text = placeResultsArray[indexPath.row].name
            
       
            return cell
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
        if let destination = segue.destination as? profileViewController {
            
            destination.searchedUser = userResultsArray[(tableView.indexPathForSelectedRow?.row)!]
            
        }
        
        if let destination = segue.destination as? CommentsTableViewController {
            
            destination.searchPost = postResultsArray[(tableView.indexPathForSelectedRow?.row)!]
            
        }
        
    }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
