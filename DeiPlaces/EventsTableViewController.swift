//
//  EventsTableViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 01/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class EventsTableViewController: UITableViewController, eventsDelegate, invitedEventsDelegate {
    
    
    var createdEventsArray = [createdEventsData]()
    var invitedEventsArray = [invitedEventsData]()
    
    var postActivity:UIActivityIndicatorView = UIActivityIndicatorView()
    
    var reportId : String?
    var postIdentification : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //fetch places
        DispatchQueue.main.async(execute: {
            self.startAnimating()
            self.loadEvents()
            self.stopAnimating()
        })
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    func loadEvents(){
        
        userID = UserDefaults.standard.object(forKey: "userId") as? String
        
        let userNum = userID!
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/user/user_events?user_id=\(userNum)")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userEvent = try JSONDecoder().decode(userEvents.self, from: data)
                
                //print(userPosts.data)
                //print("it works")
                let userData1 = userEvent.data
                let eventData = userData1.events
                
                
                if eventData?.created_events != nil {
                    self.createdEventsArray = (eventData?.created_events)!
                    
                    
                    //iterate through array to get specific values from struct
                    if self.createdEventsArray.isEmpty{
                        print("You don't have any events")
                    }else{
                        for eventsArray in self.createdEventsArray{
                            
                            print(eventsArray)
                            
                            DispatchQueue.main.async(execute: {
                                self.tableView.reloadData()
                            })
                            
                            
                        }
                    }
                }else{
                    print("You haven't created any events")
                }
                
           
             
                if eventData?.invited_events != nil {
                    self.invitedEventsArray = (eventData?.invited_events)!
                    
                    if self.invitedEventsArray.isEmpty{
                        print("You haven't been invited to any events")
                    }else{
                        for invitedeventArray in self.invitedEventsArray{
                            
                            print(invitedeventArray)
                            DispatchQueue.main.async(execute: {
                                self.tableView.reloadData()
                            })
                            
                        }
                    }
                }else{
                    print("You haven't been invited to any events")
                }
             
             
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    //set title for sections in tableview
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
            return ""
        }else if section == 1 {
            return "My Events"
        }else{
            return "My Invitations "
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0{
            return 1
            
        }else if section == 1{
           return createdEventsArray.count
            
        }else{
             return invitedEventsArray.count
        }
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addEvent", for: indexPath) as! AddEventsCell
            
            return cell
        
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "eventsCell", for: indexPath) as! EventsCell
            
            cell.eventsName.text = createdEventsArray[indexPath.row].name
            cell.eventsOwner.text = createdEventsArray[indexPath.row].first_name
            cell.eventsStartdate.text = createdEventsArray[indexPath.row].start_datetime
            cell.eventsInfo.text = createdEventsArray[indexPath.row].about
            cell.eventsEnddate.text = createdEventsArray[indexPath.row].end_datetime
            
            //load post picture from server library
            //var createdEventImageName : String?
            if createdEventsArray[indexPath.row].profile_pic_filename != nil{
                let createdImage = createdEventsArray[indexPath.row].profile_pic_filename
                
                let urlPostImageString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(createdImage)!
                let eventsImageUrl = URL(string: urlPostImageString)
                cell.eventsImage.downloadedFrom(url: eventsImageUrl!)
                
                cell.eventsImage?.layer.cornerRadius = 8.0
                cell.eventsImage?.clipsToBounds = true
            }else{
                print("event has no picture")
            }
            // Configure the cell...
            
            cell.delegate = self
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "InvitedEventsCell", for: indexPath) as! InvitedEventsCell
            
            cell.invitedName.text = invitedEventsArray[indexPath.row].name
            cell.invitedEventName.text = invitedEventsArray[indexPath.row].first_name
            cell.invitedStartDate.text = invitedEventsArray[indexPath.row].start_datetime
            cell.invitedAbout.text = invitedEventsArray[indexPath.row].about
            cell.invitedEndDate.text = invitedEventsArray[indexPath.row].end_datetime
            
            //load post picture from server library
            //var createdEventImageName : String?
            if invitedEventsArray[indexPath.row].profile_pic_filename != nil{
                let invitedImage = invitedEventsArray[indexPath.row].profile_pic_filename
                
                let urlPostImageString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(invitedImage)!
                let eventsImageUrl = URL(string: urlPostImageString)
                cell.invitedImage.downloadedFrom(url: eventsImageUrl!)
                
                cell.invitedImage?.layer.cornerRadius = 8.0
                cell.invitedImage?.clipsToBounds = true
            }else{
                print("event has no picture")
            }
            // Configure the cell...
            cell.delegate = self
            
            return cell
            
        }
        
        
    }
  
    func didTapMore(id: String){
        
        postIdentification = id
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        let reportAction = UIAlertAction(title: "Report this event", style: .default, handler: self.sendHandlerNoImage)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)

        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil )
        
    }
    
    func sendHandler(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Report this event to DeiPlaces", message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "It's Spam", style: .default, handler: { action in
            self.reportId = "1"
        }))
        
        alertController.addAction(UIAlertAction(title: "Nudity", style: .default, handler: { action in
            self.reportId = "2"
        }))
        
        alertController.addAction(UIAlertAction(title: "Violence", style: .default, handler: { action in
            self.reportId = "3"
        }))
        
        alertController.addAction(UIAlertAction(title: " Harassment", style: .default, handler: { action in
            self.reportId = "5"
        }))
        
        alertController.addAction(UIAlertAction(title: "False News", style: .default, handler: { action in
            self.reportId = "5"
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
        }))
        
        self.present(alertController, animated: true, completion: nil )
    }
    

    func didTapMoreNoImage(id: String){
        
        postIdentification = id
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
      
        let reportAction = UIAlertAction(title: "Report this event", style: .default, handler: self.sendHandlerNoImage)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)

        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil )
        
    }
    
    func sendHandlerNoImage(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Report this event to DeiPlaces", message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "It's Spam", style: .default, handler: { action in
            self.reportId = "1"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Nudity", style: .default, handler: { action in
            self.reportId = "2"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Violence", style: .default, handler: { action in
            self.reportId = "3"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: " Harassment", style: .default, handler: { action in
            self.reportId = "4"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "False News", style: .default, handler: { action in
            self.reportId = "5"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
            self.sendReport()
        }))
        
        self.present(alertController, animated: true, completion: nil )
    }
    
    func thanksNoImage(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Thanks for your feedback", message: "We have a team ready to review this report and act accordingly", preferredStyle: .alert)
        
        
        let thanksAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alertController.addAction(thanksAction)
        
        self.present(alertController, animated: true, completion: nil )
        
    }
    
    
    func sendReport(){
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        let target_type_id = "4"
        let target_id = postIdentification
        let creator_id = userId
        let reasonID  = reportId
        let messageString = ""
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/report-content")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "target_type_id=\(target_type_id)&target_id=\(target_id!)&creator_id=\(creator_id)&reason_id=\(reasonID!)&message=\(messageString)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        let displayMessage = parseJSON["data"]
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            let messageCustom = displayMessage
                            appDelegate.infoView(message: messageCustom as! String, color: colorBrandBlue)
                            
                        }else{
                            
                            self.tableView.reloadData()
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                            
                            print(error)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
        
        
    }
    
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func startAnimating() {
        postActivity.center = self.view.center
        
        postActivity.hidesWhenStopped = true
        postActivity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(postActivity)
        
        postActivity.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        postActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
}
