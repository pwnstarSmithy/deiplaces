//
//  CommentsTableViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 15/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class CommentsTableViewController: UITableViewController, commentPostCellDelegate, commentNoImageCellDelegate{
    
    var loginActivity:UIActivityIndicatorView = UIActivityIndicatorView()
    
    var reportId : String?
    var postIdentification : String?
    
    var postDetails : postData?
    var searchPost : postResults?
    var commentCount : Int?
    var likeCount : Int?
    var postsArray = [postData]()
    var singlePostDic : singlePostData?
    
    var postId : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startAnimating()
        reloadPosts()
        stopAnimating()
        tableView.separatorStyle = .none
        
        // dynamic table view cell height
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        print("Comment VC")
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0{
            return 1
            
        }else if section == 1{
            return 1
            
        }else{
            if singlePostDic?.comments != nil{
                return (singlePostDic?.comments!.count)!
            }else{
                return 0
            }
            
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            
            if singlePostDic?.photos != nil{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "PostCellComment", for: indexPath) as! commentPostTableViewCell
                
                
                if singlePostDic?.comments != nil {
                    
                    commentCount = (singlePostDic?.comments?.count)!
                }else {
                    
                    commentCount = 0
                }
                
                if singlePostDic?.like_list != nil {
                    
                    likeCount = (singlePostDic?.like_list?.count)!
                    
                }else{
                    
                    likeCount = 0
                }
                
                //assign post id to PostID
                postID = singlePostDic?.post_id
                /*
                 //make username clickable!
                 let tap = UITapGestureRecognizer(target: self, action: #selector(NewsfeedTableViewController.tapFunction))
                 cell.usernameLabel.isUserInteractionEnabled = true
                 cell.usernameLabel.addGestureRecognizer(tap)
                 */
                
                
                cell.usernameLabel.text = singlePostDic?.fullname
                cell.timeAgoLabel.text = singlePostDic?.data_created
                cell.captionLabel.text = singlePostDic?.content
                cell.timeAgoLabel.text = singlePostDic?.modified
                cell.postID.text = singlePostDic?.post_id
                //15 Likes     30 Comments     500 Shares
                cell.postStatsLabel.text = "\(likeCount!) Likes      \(commentCount!)  Comments"
                //load profile picture from library
                if singlePostDic?.profile_pic_filename != nil {
                    let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(singlePostDic?.profile_pic_filename)!
                    let profileURL = URL(string: urlString)
                    
                    cell.profileImageView.downloadedFrom(url: profileURL!)
                }else{
                    print("User has no profile picture!")
                }
                
                
                //iterate through posts images images array
                
                //load post picture from server library
                var postImageName : String?
                if singlePostDic?.photos != nil{
                    let postImage = singlePostDic?.photos
                    
                    for postsImage in postImage! {
                        
                        postImageName = postsImage.filename!
                    }
                        
                   
                      
                    let urlPostImageString = "https://deiplaces.com/uploads/post-picture/image_500/"+(postImageName)!
                    let postsImageUrl = URL(string: urlPostImageString)
                    cell.postImageView.downloadedFrom(url: postsImageUrl!)
                    cell.postImageView?.layer.cornerRadius = 8.0
                    cell.postImageView?.clipsToBounds = true
                }else{
                    print("Post has no picture")
                }
                cell.delegate = self
                //return cell
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoImageCellComment", for: indexPath) as! CommentNoImagePostTableViewCell
                
                if singlePostDic?.comments != nil {
                    
                    commentCount = (singlePostDic?.comments?.count)!
                }else {
                    
                    commentCount = 0
                }
                
                if singlePostDic?.like_list != nil {
                    
                    likeCount = (singlePostDic?.like_list?.count)!
                    
                }else{
                    
                    likeCount = 0
                }
                /*
                 //make username clickable!
                 let tap = UITapGestureRecognizer(target: self, action: #selector(NewsfeedTableViewController.tapFunction))
                 cell.noImageUsername.isUserInteractionEnabled = true
                 cell.noImageUsername.addGestureRecognizer(tap)
                 
                 */
                
                
                cell.noImageUsername.text = singlePostDic?.fullname
                cell.noImageTime.text = singlePostDic?.data_created
                cell.noImagePost.text = singlePostDic?.content
                cell.noImageTime.text = singlePostDic?.modified
                cell.postID.text = singlePostDic?.post_id
                //15 Likes     30 Comments     500 Shares
                cell.noImageLikeAndComment.text = "\(likeCount!) Likes      \(commentCount!)  Comments"
                
                //load profile picture from library
                if postDetails?.profile_pic_filename != nil {
                    let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(postDetails?.profile_pic_filename)!
                    let profileURL = URL(string: urlString)
                    
                    cell.noImageProfilePic.downloadedFrom(url: profileURL!)
                    cell.noImageProfilePic?.layer.cornerRadius = 8.0
                    cell.noImageProfilePic?.clipsToBounds = true
                }else{
                      print("you have no profile picture set")
                }
                cell.delegate = self
                return cell
                
            }
            
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddComment", for: indexPath) as! AddCommentTableViewCell
            
            //give textfield round corners
            cell.commentNow.layer.cornerRadius = cell.commentNow.bounds.width / 50
            
            cell.sendButton.addTarget(self, action: #selector(CommentsTableViewController.postComment(_:)), for: .touchUpInside)
            
            //cell.commentPressed(postComment())
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DisplayComments", for: indexPath) as! ViewCommentTableViewCell
            
          let martha = singlePostDic?.comments
            var marthaArray = [singleComment]()
            
            marthaArray = martha!
            
            for marthasArray in marthaArray{
                
                print(marthasArray)
                var commentContent : String?
                var commentUsername : String?
                var commentProfilePicture : String?
                
                commentContent = marthaArray[indexPath.row].content
                commentUsername = marthaArray[indexPath.row].fullname
                commentProfilePicture = marthaArray[indexPath.row].profile_pic_filename
                
                cell.showComments.text = commentContent
                cell.commentUsername.text = commentUsername
                
                if commentProfilePicture != nil{
                    //load profile picture from library
                    let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(commentProfilePicture)!
                    let profileURL = URL(string: urlString)
                    
                    cell.commentProfilePic.downloadedFrom(url: profileURL!)
                    
                    cell.commentProfilePic?.layer.cornerRadius = 8.0
                    cell.commentProfilePic?.clipsToBounds = true
                    
                }else{
                    //do nothing if no profile pic
                }
                cell.showComments.layer.cornerRadius = cell.showComments.bounds.width / 50
                cell.commentProfilePic.layer.cornerRadius = cell.commentProfilePic.layer.bounds.width / 50
                cell.commentUsername.layer.cornerRadius = cell.commentUsername.layer.bounds.width / 50
                
            }
            // Configure the cell...
            
            return cell
            
        }
      
    }
    //function to post comments
    @objc func postComment(_ sender: Any?) {
        startAnimating()
        print("send tapped", sender)
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        var commentString : String?
        
        let indexpathForComment = NSIndexPath(row: 0, section: 1)
        let commentCell = tableView.cellForRow(at: indexpathForComment as IndexPath)! as! AddCommentTableViewCell
    
        
        if commentCell.commentNow.text.isEmpty {
            
            let alert = UIAlertController(title: "Alert", message: "all fields are required to be filled in", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            
        }
        
        commentString = commentCell.commentNow.text
        
        //variables to be passed to server
        let target_id = postDetails?.post_id
        let creator_id = userId
        
        if commentString != nil {
            let commentPost = commentString
            
            // url path to php file
            let url = URL(string: "https://deiplaces.com/api/index.php/comments/add-comment")!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            let body = "target_type_id=\(4)&target_id=\(target_id!)&creator_id=\(creator_id)&content=\(commentPost!)"
            
            request.httpBody = body.data(using: String.Encoding.utf8)
            // launch session
            URLSession.shared.dataTask(with: request) { data, response, error in
                
                // get main queu to communicate back to user
                DispatchQueue.main.async(execute: {
                    
                    if error == nil {
                        
                        do {
                            
                            // json containes $returnArray from php
                            let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                            
                            // declare new var to store json inf
                            guard let parseJSON = json else {
                                print("Error while parsing")
                                return
                            }
                            
                            // get message from $returnArray["message"]
                            let message = parseJSON["message"]
                            
                            // if there is some message - post is made
                            if message != nil {
                                DispatchQueue.main.async(execute: {
                                    let message = "Comment posted"
                                    
                                    
                                    commentCell.commentNow.text = ""
                                    appDelegate.infoView(message: message, color: colorBrandBlue)
                                    //self.tableCommunity.reloadRows(at: [indexPath], with: .none)
                                   
                                    //let indexPath = IndexPath(item: tag, section: 0)
                                    //let indexpathForComment = NSIndexPath(row: 0, section: 1)
                                    //self.tableView.reloadRows(at: [indexpathForComment as IndexPath], with: .none)
                                    //let indexpathForPostedComments = NSIndexPath(row: 0, section: 2)
                                    //let indexSetForComments = NSIndexSet(index: 2)
                                    //self.tableView.reloadRows(at: [indexpathForPostedComments as IndexPath], with: .fade)
                                    //self.tableView.reloadSections(indexSetForComments as IndexSet, with: .fade)
                                  
                                   self.stopAnimating()
                                    self.reloadPosts()
                                    
                                    let commentSection = self.singlePostDic?.comments
                                    
                                    if commentSection != nil {
                                        var indexPathsToReload = [IndexPath]()
                                        for index in commentSection!.indices {
                                            
                                            let indexPath = IndexPath(row: index, section: 2)
                                            indexPathsToReload.append(indexPath)
                                        }
                                        self.tableView.reloadRows(at: indexPathsToReload, with: .right)
                                    }else{
                                        
                                        self.tableView.reloadData()
                                        
                                        
                                    }
                                 
                        
                                })
                               
                            }
                            
                        } catch {
                            self.stopAnimating()
                            // get main queue to communicate back to user
                            DispatchQueue.main.async(execute: {
                                let message = "\(error)"
                                appDelegate.infoView(message: message, color: colorSmoothRed)
                            })
                            return
                            
                        }
                        
                    } else {
                        self.stopAnimating()
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = error!.localizedDescription
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                    
                })
                
                }.resume()
            
            
        }else{
            //do something!
            print("Martha")
        }
      
        
    }
    func reloadPosts() {
        
        if postDetails?.post_id != nil {
            postId = postDetails?.post_id
        }else{
            postId = searchPost?.post_id
        }
        
        
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/posts/single_post?post_id=\(postId!)")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userPosts = try JSONDecoder().decode(singlePost.self, from: data)
                
                //print(userPosts.data)
                //print("it works")
                
                //self.postsArray = userPosts.data
                
                DispatchQueue.main.async(execute: {
                    
                   self.singlePostDic = userPosts.data
                   self.tableView.reloadData()
                    
                                       })
                
                //iterate through array to get specific values from struct
                
//                for postArray in self.postsArray{
//                    //print(postArray.content)
//
//
//
//
//
//
//                }
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
        
    }
    func startAnimating() {
        loginActivity.center = self.view.center
        
        loginActivity.hidesWhenStopped = true
        loginActivity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(loginActivity)
        
        loginActivity.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        loginActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
    func didLike(id: String){
        postIdentification = id
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        //variables to be passed to server
        
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/add_post_reaction")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "react_id=\(1)&user_id=\(userId)&post_id=\(postIdentification!)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        //print(message)
                        
                        // if there is some message - post is made
                        if message != nil {
                            DispatchQueue.main.async(execute: {
                                self.reloadPosts()
                                //reload Stats label
                                self.tableView.reloadData()
                            })
                            
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
    }
    func didLikeNoImage(id: String){
        postIdentification = id
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        //variables to be passed to server
        
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/add_post_reaction")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "react_id=\(1)&user_id=\(userId)&post_id=\(postIdentification!)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        //print(message)
                        
                        // if there is some message - post is made
                        if message != nil {
                            DispatchQueue.main.async(execute: {
                                self.reloadPosts()
                                //reload Stats label
                                self.tableView.reloadData()
                            })
                            
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
    }
    func didTapMore(id: String){
        
        postIdentification = id
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        
        let unfollowAction = UIAlertAction(title: "Hide this post", style: .default, handler: self.sendUnfollowHandler)
        let reportAction = UIAlertAction(title: "Report this post", style: .default, handler: self.sendHandlerNoImage)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(unfollowAction)
        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil )
        
    }
    
    func sendHandler(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Report this post to DeiPlaces", message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "It's Spam", style: .default, handler: { action in
            self.reportId = "1"
        }))
        
        alertController.addAction(UIAlertAction(title: "Nudity", style: .default, handler: { action in
            self.reportId = "2"
        }))
        
        alertController.addAction(UIAlertAction(title: "Violence", style: .default, handler: { action in
            self.reportId = "3"
        }))
        
        alertController.addAction(UIAlertAction(title: " Harassment", style: .default, handler: { action in
            self.reportId = "5"
        }))
        
        alertController.addAction(UIAlertAction(title: "False News", style: .default, handler: { action in
            self.reportId = "5"
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
        }))
        
        self.present(alertController, animated: true, completion: nil )
    }
    func didTapMoreNoImage(id: String){
        
        postIdentification = id
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        let unfollowAction = UIAlertAction(title: "Unfollow user", style: .default, handler: self.sendUnfollowHandler)
        let reportAction = UIAlertAction(title: "Report this post", style: .default, handler: self.sendHandlerNoImage)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(unfollowAction)
        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil )
        
    }
    
    func sendUnfollowHandler(alert: UIAlertAction!){
        
        unfollow()
        
        let alertController = UIAlertController(title: "Successful", message: "You won't see that post anymore!", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        
        self.present(alertController, animated: true, completion: nil )
    }
    
    func sendHandlerNoImage(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Report this post to DeiPlaces", message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "It's Spam", style: .default, handler: { action in
            self.reportId = "1"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Nudity", style: .default, handler: { action in
            self.reportId = "2"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Violence", style: .default, handler: { action in
            self.reportId = "3"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: " Harassment", style: .default, handler: { action in
            self.reportId = "4"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "False News", style: .default, handler: { action in
            self.reportId = "5"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
            self.sendReport()
        }))
        
        self.present(alertController, animated: true, completion: nil )
    }
    
    func thanksNoImage(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Thanks for your feedback", message: "We have a team ready to review this report and act accordingly", preferredStyle: .alert)
        
        
        let thanksAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alertController.addAction(thanksAction)
        
        self.present(alertController, animated: true, completion: nil )
        
    }
    func performSegueToReturnBack()  {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    func unfollow(){
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        let target_type_name = "post"
        let target_id = postIdentification
        
        print(target_id!)
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/hide-content")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "target_type_name=\(target_type_name)&target_id=\(target_id!)&creator_id=\(userId)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        let displayMessage = parseJSON["data"]
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            let messageCustom = displayMessage
                            appDelegate.infoView(message: messageCustom as! String, color: colorBrandBlue)
                          
                            self.performSegueToReturnBack()
                         
                        }else{
                            
                            self.tableView.reloadData()
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                            
                            print(error)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
    }
    
    func sendReport(){
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        let target_type_id = "post"
        let target_id = postIdentification
        let creator_id = userId
        let reasonID  = reportId
        let messageString = ""
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/report-content")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "target_type_id=\(target_type_id)&target_id=\(target_id!)&creator_id=\(creator_id)&reason_id=\(reasonID!)&message=\(messageString)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        let displayMessage = parseJSON["data"]
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            let messageCustom = displayMessage
                            appDelegate.infoView(message: messageCustom as! String, color: colorBrandBlue)
                            
                        }else{
                            
                            self.tableView.reloadData()
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                            
                            print(error)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
        
        
    }
    
}


  

