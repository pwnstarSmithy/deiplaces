//
//  changePassViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 23/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class changePassViewController: UIViewController {

    @IBOutlet weak var oldPass: UITextField!
    
    @IBOutlet weak var newPass: UITextField!
    
    @IBOutlet weak var confirmPass: UITextField!
    
    @IBAction func saveClicked(_ sender: Any) {
        
        saveData()
    }
    
    var oldPassword : String?
    var newPassword : String?
    var confirmPassword : String?
    
    func saveData(){
        
        if oldPass.text != nil {
            
            oldPassword = oldPass.text
            
        }else{
            
        }
        
        if newPass.text != nil {
            
            newPassword = newPass.text
            
        }else{
            
            
        }
        
        if confirmPass.text != nil {
            
            confirmPassword = confirmPass.text
            
        }else{
            
            
            
        }
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        //send request to server
        let url = NSURL(string: "https://deiplaces.com/api/index.php/settings/change_password")!
        
        
        
        //request url
        var request = URLRequest(url: url as URL)
        
        // method to pass data
        request.httpMethod = "POST"
        let body = "old_password=\(oldPassword!)&new_password=\(newPassword!)&confirm_password=\(confirmPassword!)&user_id=\(userId)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        //launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else{return}
            // check if no error
            if error == nil{
                
                do{
                    
                    //let json = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    //print(json)
                    
                    let userDetails = try JSONDecoder().decode(user.self, from: data)
                    
                    //print(userDetails.data.fullname)
                    let statusMessage = userDetails.status
                    
                    if statusMessage == "true"{
                        let displayMessage = userDetails.message
                        
                          appDelegate.infoView(message: displayMessage, color: colorBrandBlue)
                    }else{
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                        
                            
                            let message = userDetails.message
                            
                            let failAlert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            
                            failAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                                
                                failAlert.dismiss(animated: true, completion: nil)
                                
                                
                            }))
                            
                            DispatchQueue.main.async(execute: {
                                self.present(failAlert, animated: true, completion: nil)
                            })
                            
                        })
                        
                        
                        
                    }
                    
                    
                }catch {
                    
                    
                    DispatchQueue.main.async(execute: {
                    
                        //let message = "\(error)"
                        appDelegate.infoView(message: "Check your details", color: colorSmoothRed)
                        print(error)
                    })
                    return
                }
                
            }else{
              
                return
            }
            
            
            }.resume()
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
