//
//  loadingViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 10/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class loadingViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startAnimating()
        //assign userid from saved information
        if  UserDefaults.standard.object(forKey: "userId") != nil {
              //let userId = UserDefaults.standard.object(forKey: "userId") as! String
            // go to tabbar / home page
            DispatchQueue.main.async(execute: {
                //function to stop activity indicator
                self.stopAnimating()
                
                appDelegate.login()
            })
            
            
        }else{
            
            // go to walkthrough pages
            DispatchQueue.main.async(execute: {
                //function to stop activity indicator
                self.stopAnimating()
                
                appDelegate.pageController()
            })
            
        }
        
      
        
    
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startAnimating() {
        //loginActivity.center = self.view.center
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
