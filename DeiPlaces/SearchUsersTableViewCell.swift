//
//  SearchUsersTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 25/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class SearchUsersTableViewCell: UITableViewCell {

    @IBOutlet weak var userLabel: UILabel!
    
    @IBOutlet weak var userImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
