//
//  PostCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 24/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

protocol PostCellDelegate {
    func didTapMore(id: String)
    func didLike(id: String)
}

class PostCell : UITableViewCell
{
    @IBOutlet weak var timeAgoLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var postStatsLabel: UILabel!
    
    @IBAction func likePressed(_ sender: Any) {
        
        delegate?.didLike(id: postID.text!)
        
    }
    
    
    
    @IBOutlet weak var postID: UILabel!
    var delegate: PostCellDelegate?
    
    @IBAction func moreTapped(_ sender: Any) {
        
        delegate?.didTapMore(id: postID.text!)
        
    }
    
}
    
    
    


    /*
    @IBAction func likeButton(_ sender: Any) {
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        //variables to be passed to server
        
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/add_post_reaction")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "react_id=\(1)&user_id=\(userId)&post_id=\(postID!)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        //print(message)
                        
                        // if there is some message - post is made
                        if message != nil {
                            
          
                            //reload Stats label

                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
        
        
    }
    
    */
    
    






