//
//  NoImageTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 03/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

protocol NoImageDelegate {
    func didTapMoreNoImage(id: String)
    func didLikeNoImage(id: String)
}

class NoImageTableViewCell: UITableViewCell {

    @IBOutlet weak var noImageProfilePic: UIImageView!
    
    @IBOutlet weak var noImageUsername: UILabel!
    
    @IBOutlet weak var noImageTime: UILabel!
    
    @IBOutlet weak var noImagePost: UILabel!
    
    @IBOutlet weak var noImageLikeAndComment: UILabel!
    
    @IBAction func likePressed(_ sender: Any) {
        delegate?.didLikeNoImage(id: postID.text!)
        
    }
    
    var delegate: NoImageDelegate?
    
    @IBOutlet weak var postID: UILabel!
    
    @IBAction func moreTapped(_ sender: Any) {
        delegate?.didTapMoreNoImage(id: postID.text!)
        
    }
}
