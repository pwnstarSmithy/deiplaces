//
//  NewsfeedTableViewController.swift
//
//
//  Created by pwnstarSmithy
//  Copyright © Bulamu.NET
//

import UIKit

//Declare variable to cache Newsfeed images.
var postImageCache = NSCache<NSString, UIImage>()

//extension to download images from internet
extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        //make sure the cell is emptied before checking cache for  image
        image = nil
        //check if image is in cache before loading it from the internet
        if let imageFromCache = postImageCache.object(forKey: url.absoluteString as NSString){
            
            self.image = imageFromCache
            return
        }
        
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                
                let imageToCache = image
                
                postImageCache.setObject(imageToCache, forKey: url.absoluteString as NSString)
                
                self.image = imageToCache
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}




class NewsfeedTableViewController : UITableViewController
{
     var loginActivity:UIActivityIndicatorView = UIActivityIndicatorView()

    //var searchBar: UISearchBar!
    
    var postCount = 0
    
    var postsArray = [postData]()
    
    var userResultsArray = [userResults]()
    var placeResultsArray = [placeResults]()
    var postResultsArray = [postResults]()
    var eventResultsArray = [eventResults]()
    var searchResult : String?
    
    var creatorData : String?
  
    var reportId : String?
    var postIdentification : String?
        var reportTextField: UITextField?
    
    var likes : Int?
    var comments : Int?
    
    var userInfo : userData?
    
    var profilePicture : String?
    var coverPicture : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if firebaseToken != nil {
            //send firebaseID to the server everytime the app loads.
            sendFirebaseToken()
            print("Firebase ID sent!")
        }else{
            print("Device has no firebase token!")
        }
      
        //fetch posts!
        DispatchQueue.main.async(execute: {
            self.loadPosts()

            self.loadUser()
        })
  
        
        tableView.separatorStyle = .none
        
        // dynamic table view cell height
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
      
    }
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            self.loadPosts()
            self.tableView.reloadData()
        })
    }

    
    //do something when label is tapped
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        
        //segue to other view when username tapped
        // refer to our Main.storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        // store our tabBar Object from Main.storyboard in tabBar var
        let profileView = storyboard.instantiateViewController(withIdentifier: "profileView")
        
        self.navigationController?.pushViewController(profileView, animated: true)
        //self.present(profileView, animated: true, completion: nil)
        
        //appDelegate.profileView()
        
        print("tap working")
    }
    
    
    func startAnimating() {
        loginActivity.center = self.view.center
        
        loginActivity.hidesWhenStopped = true
        loginActivity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(loginActivity)
        
        loginActivity.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        loginActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }

}

// MARK: - UITableViewDataSource

extension NewsfeedTableViewController
{
    
    func sendFirebaseToken(){
        
        print(firebaseToken!)
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        //send request to server
        let url = NSURL(string: "https://deiplaces.com/api/index.php/androidNotifications/update-user-device-token")!
        
        //request url
        var request = URLRequest(url: url as URL)
        
        // method to pass data
        request.httpMethod = "POST"
        let body = "token=\(firebaseToken!)&user_id=\(userId)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        //launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else{return}
            // check if no error
            if error == nil{
                
                do{
                    
                    //let json = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    //print(json)
                    
                    let userDetails = try JSONDecoder().decode(standard.self, from: data)
                    
                    let status = userDetails.status
                    
                    if status == "true" {
                        print("firebase ID updated")
                    }
                    
                }catch {
                    
             
                    return
                }
                
            }else{
                
                return
            }
            
            
        }
        
    }
    
    //function to fetch posts from the internet and load them!
    func loadPosts(){
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/posts/user-newsfeed-posts?user_id=\(userId)")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userPosts = try JSONDecoder().decode(deiPost.self, from: data)
                
                //print(userPosts.data)
                //print("it works")
                
                self.postsArray = userPosts.data
                //iterate through array to get specific values from struct
                
                for postArray in self.postsArray{
                    //print(postArray.content)
                    print(postArray)
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                    
                    
                }
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
    }
    
    func load10Posts(){
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/posts/user-newsfeed-posts?user_id=\(userId)&start_row=\(postCount)")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userPosts = try JSONDecoder().decode(deiPost.self, from: data)
                
                //print(userPosts.data)
                //print("it works")
                
                let testArray = userPosts.data
                
                
                
                for postArray in testArray{
                    //print(postArray.content)
                    
                    self.postsArray.append(postArray)
                    
                    
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                    
                    
                }
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
    }
    func load20Posts(){
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/posts/user-newsfeed-posts?user_id=\(userId)&start_row=20")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userPosts = try JSONDecoder().decode(deiPost.self, from: data)
                
                //print(userPosts.data)
                //print("it works")
                
                let testArray = userPosts.data
                
                
                //                self.postsArray = userPosts.data
                //                //iterate through array to get specific values from struct
                //
                //                self.postsArray.append(userPosts.data)
                
                
                for postArray in testArray{
                    //print(postArray.content)
                    
                    self.postsArray.append(postArray)
                    
                    
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                    
                    
                }
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
    }
    
    func loadUser(){
        
        //
        userID = UserDefaults.standard.object(forKey: "userId") as? String
        
        let userNum = userID!
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/user/single-user?user_id=\(userNum)")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userInfor = try JSONDecoder().decode(user.self, from: data)
                
                //print(userPosts.data)
                //print("it works")
                
                self.userInfo = userInfor.data
                DispatchQueue.main.async(execute: {
                    let coverPicFile = self.userInfo?.cover_pic_filename
                    
                    if coverPicFile != nil {
                        self.coverPicture = coverPicFile
                        
                    }else{
                        print("You don't have a profile pic!")
                    }
                    
                    let profilePicFile = self.userInfo?.profile_pic_filename
                    
                    if profilePicFile != nil {
                        
                        self.profilePicture = profilePicFile
                        
                    }else{
                        print("You have no profile picture!")
                    }
                })
                
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
                return 1
            
        }else{
                return postsArray.count
           
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //var returnCell: UITableViewCell!
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "statusCell", for: indexPath) as! statusCell
         
            if profilePicture != nil {
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(profilePicture)!
                let profileURL = URL(string: urlString)
                
                cell.statusProfilePic?.downloadedFrom(url: profileURL!)
                
                cell.statusProfilePic?.layer.cornerRadius = 20
                cell.statusProfilePic?.clipsToBounds = true
                
                }else{
                
                print("you have no profile picture set")
                
            }
  
            return cell
            
        }else {
            
            if postsArray[indexPath.row].photos != nil{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
            
                
                if postsArray[indexPath.row].comments != nil {
                    
                    comments = postsArray[indexPath.row].comments?.count
                }else {
                    
                    comments = 0
                }
                
                if postsArray[indexPath.row].like_list != nil {
                    
                    likes = postsArray[indexPath.row].like_list?.count
                    
                }else{
                    
                    likes = 0
                }
                
                //assign post id to PostID
                postID = postsArray[indexPath.row].post_id
                
                //make username clickable!
                let tap = UITapGestureRecognizer(target: self, action: #selector(NewsfeedTableViewController.tapFunction))
                cell.usernameLabel.isUserInteractionEnabled = true
                cell.usernameLabel.addGestureRecognizer(tap)
                
                for martha in postsArray{
                    
                    userClicked = martha.creator_id
                    
                }
            
            cell.usernameLabel.text = postsArray[indexPath.row].fullname
            cell.timeAgoLabel.text = postsArray[indexPath.row].data_created
            cell.captionLabel.text = postsArray[indexPath.row].content
            cell.timeAgoLabel.text = postsArray[indexPath.row].modified
            cell.postID.text = postsArray[indexPath.row].post_id
                //15 Likes     30 Comments     500 Shares
            cell.postStatsLabel.text = "\(likes!) Likes      \(comments!)  Comments"
            //load profile picture from library

                if postsArray[indexPath.row].profile_pic_filename != nil{
                    //load profile picture from library
                    let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(postsArray[indexPath.row].profile_pic_filename)!
                    let profileURL = URL(string: urlString)
                    
                     cell.profileImageView.downloadedFrom(url: profileURL!)
                    cell.profileImageView?.layer.cornerRadius = 8.0
                    cell.profileImageView?.clipsToBounds = true
                }else {
                    print("User has no profile pic")
                }
                
            //iterate through posts images images array
            
            //load post picture from server library
            var postImageName : String?
            if postsArray[indexPath.row].photos != nil{
                let postImage = postsArray[indexPath.row].photos
                for postsImage in postImage!{
                    
                    postImageName = postsImage.filename!
                    
                }
                let urlPostImageString = "https://deiplaces.com/uploads/post-picture/image_500/"+(postImageName)!
                let postsImageUrl = URL(string: urlPostImageString)
                cell.postImageView.downloadedFrom(url: postsImageUrl!)
            }else{
                print("Post has no picture")
            }
            //return cell
                cell.delegate = self
            return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoImageCell", for: indexPath) as! NoImageTableViewCell
                
                if postsArray[indexPath.row].comments != nil {
                    
                    comments = postsArray[indexPath.row].comments?.count
                }else {
                    
                    comments = 0
                }
                
                if postsArray[indexPath.row].like_list != nil {
                    
                likes = postsArray[indexPath.row].like_list?.count
                    
                }else{
                    
                 likes = 0
                }
                /*
                //make username clickable!
                let tap = UITapGestureRecognizer(target: self, action: #selector(NewsfeedTableViewController.tapFunction))
                cell.noImageUsername.isUserInteractionEnabled = true
                cell.noImageUsername.addGestureRecognizer(tap)
                
                
                for martha in postsArray{
                    
                    userClicked = martha.creator_id
                    
                }
                */
                
                cell.noImageUsername.text = postsArray[indexPath.row].fullname
                cell.noImageTime.text = postsArray[indexPath.row].data_created
                cell.noImagePost.text = postsArray[indexPath.row].content
                cell.noImageTime.text = postsArray[indexPath.row].modified
                cell.postID.text = postsArray[indexPath.row].post_id
                //15 Likes     30 Comments     500 Shares
                cell.noImageLikeAndComment.text = "\(likes!) Likes      \(comments!)  Comments"
                
                if postsArray[indexPath.row].profile_pic_filename != nil{
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_80/"+(postsArray[indexPath.row].profile_pic_filename)!
                let profileURL = URL(string: urlString)
                
                cell.noImageProfilePic.downloadedFrom(url: profileURL!)
                    cell.noImageProfilePic?.layer.cornerRadius = 8.0
                    cell.noImageProfilePic?.clipsToBounds = true
                }else {
                    print("User has no profile pic")
                }
                
                cell.delegate = self
                return cell
                
            }
            
        }
        
            
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == postsArray.count - 1 {
            // load more data from the server
            
            //check if the number of cells is divisble by 10
            if postsArray.count % 10 == 0 {
                //add 10 to the original number of posts
                postCount += 10
                //call the function to load posts
                load10Posts()
                
            }
            
//            if postsArray.count == 10{
//
//                load10Posts()
//            }else if postsArray.count == 20 {
//                load20Posts()
//            }
//

        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
        
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? CommentsTableViewController {
            
            destination.postDetails = postsArray[(tableView.indexPathForSelectedRow?.row)!]
            
        }
    }
    
    
}

extension NewsfeedTableViewController: PostCellDelegate {
    
    func didLike(id: String){
        
       postIdentification = id
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        //variables to be passed to server
        
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/add_post_reaction")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "react_id=\(1)&user_id=\(userId)&post_id=\(postIdentification!)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        //print(message)
                        
                        // if there is some message - post is made
                        if message != nil {
                            DispatchQueue.main.async(execute: {
                                self.loadPosts()
                                //reload Stats label
                                self.tableView.reloadData()
                            })
                          
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
        
    }
    
    
    func didTapMore(id: String){
        
        postIdentification = id
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        
        let unfollowAction = UIAlertAction(title: "Hide this post", style: .default, handler: self.sendUnfollowHandler)
        let reportAction = UIAlertAction(title: "Report this post", style: .default, handler: self.sendHandlerNoImage)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(unfollowAction)
        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil )
        
    }
    
    func sendUnfollowHandler(alert: UIAlertAction!){
        
        unfollow()
        
        let alertController = UIAlertController(title: "Successful", message: "You won't see that post anymore!", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        
        self.present(alertController, animated: true, completion: nil )
    }
    
    
    func sendHandler(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Report this post to DeiPlaces", message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "It's Spam", style: .default, handler: { action in
            self.reportId = "1"
        }))
        
        alertController.addAction(UIAlertAction(title: "Nudity", style: .default, handler: { action in
            self.reportId = "2"
        }))
        
        alertController.addAction(UIAlertAction(title: "Violence", style: .default, handler: { action in
            self.reportId = "3"
        }))
        
        alertController.addAction(UIAlertAction(title: " Harassment", style: .default, handler: { action in
            self.reportId = "5"
        }))
        
        alertController.addAction(UIAlertAction(title: "False News", style: .default, handler: { action in
            self.reportId = "5"
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
        }))
        
        self.present(alertController, animated: true, completion: nil )
    }
    
}
extension NewsfeedTableViewController: NoImageDelegate {
    
    func didLikeNoImage(id: String){
        
         postIdentification = id
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        //variables to be passed to server
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/add_post_reaction")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "react_id=\(1)&user_id=\(userId)&post_id=\(postIdentification!)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        //print(message)
                        
                        // if there is some message - post is made
                        if message != nil {
                            DispatchQueue.main.async(execute: {
                                self.loadPosts()
                                //reload Stats label
                                self.tableView.reloadData()
                            })
                            
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            print(message)
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
    }
    
    func didTapMoreNoImage(id: String){
        
        postIdentification = id
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)

        let unfollowAction = UIAlertAction(title: "Hide this post", style: .default, handler: self.sendUnfollowHandler)
        let reportAction = UIAlertAction(title: "Report this post", style: .default, handler: self.sendHandlerNoImage)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(unfollowAction)
        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil )
        
    }
  
    
    func sendHandlerNoImage(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Report this post to DeiPlaces", message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "It's Spam", style: .default, handler: { action in
            self.reportId = "1"
            self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Nudity", style: .default, handler: { action in
            self.reportId = "2"
             self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Violence", style: .default, handler: { action in
            self.reportId = "3"
             self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: " Harassment", style: .default, handler: { action in
            self.reportId = "4"
             self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "False News", style: .default, handler: { action in
            self.reportId = "5"
             self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Hate Speech", style: .default, handler: { action in
            self.reportId = "6"
             self.sendReport()
        }))
        
        alertController.addAction(UIAlertAction(title: "Graphic/Gore", style: .default, handler: { action in
            self.reportId = "7"
             self.sendReport()
        }))
        
        self.present(alertController, animated: true, completion: nil )
    }
    
    func thanksNoImage(alert: UIAlertAction!){
        let alertController = UIAlertController(title: "Thanks for your feedback", message: "We have a team ready to review this report and act accordingly", preferredStyle: .alert)
        
        let thanksAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alertController.addAction(thanksAction)
        
        self.present(alertController, animated: true, completion: nil )
        
    }
    
    func unfollow(){
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        let target_type_name = "post"
        let target_id = postIdentification
        
        print(target_id!)
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/hide-content")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "target_type_name=\(target_type_name)&target_id=\(target_id!)&creator_id=\(userId)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        let displayMessage = parseJSON["data"]
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            let messageCustom = displayMessage
                            appDelegate.infoView(message: messageCustom as! String, color: colorBrandBlue)
                            
                            DispatchQueue.main.async(execute: {
                                self.loadPosts()
                                //reload Stats label
                                self.tableView.reloadData()
                            })
                        }else{
                            
                            self.tableView.reloadData()
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                            
                            print(error)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
    }
    func sendReport(){
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        let target_type_id = "4"
        let target_id = postIdentification
        let creator_id = userId
        let reasonID  = reportId
        let messageString = ""
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/report-content")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "target_type_id=\(target_type_id)&target_id=\(target_id!)&creator_id=\(creator_id)&reason_id=\(reasonID!)&message=\(messageString)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        let displayMessage = parseJSON["data"]
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            let messageCustom = displayMessage
                            appDelegate.infoView(message: messageCustom as! String, color: colorBrandBlue)
                            
                        }else{
                            
                            self.tableView.reloadData()
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                            
                            print(error)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
        
        
    }
    
    
    
}
