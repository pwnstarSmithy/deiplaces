//
//  placesPostsTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 01/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class placesPostsTableViewCell: UITableViewCell {

    @IBOutlet weak var placeName: UILabel!

    @IBOutlet weak var placeAddress: UILabel!

    @IBOutlet weak var placesPhone: UILabel!

    @IBOutlet weak var placeAbout: UILabel!
    
    @IBOutlet weak var placesEmail: UILabel!
    
    @IBOutlet weak var placesCategory: UILabel!
    
}
