//
//  user.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 23/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
struct userEvents : Decodable {
    let status: String
    let message: String
    let data: userDataEvents
}

struct userDataEvents : Decodable {
    
    let user_id: String?
    let first_name: String?
    let last_name: String?
    let other_name: String?
    let fullname: String?
    let alias: String?
    let about: String?
    let sex_id: String?
    let sex: String?
    let birth_date: String?
    let relation_status_id: String?
    let relation_status: String?
    let relation_user_id: String?
    let relation_user_first_name: String?
    let relation_user_last_name: String?
    let relation_user_fullname: String?
    let location: String?
    let contact: String?
    let profile_pic: String?
    let profile_folder_name: String?
    let profile_pic_filename: String?
    let cover_pic: String?
    let cover_folder_name: String?
    let cover_pic_filename: String?
    let request_counter: String?
    let dream_destination_list: String?
    //let hidden_content_list: String?
    let email: String?
    let username: String?
    let password: String?
    let salt: String?
    let social_media_auth: String?
    let social_media_id: String?
    let date_created: String?
    let last_seen: String?
    let is_suspended: String?
    let is_notified: String?
    let approve_token: String?
    let device_token: String?
    let language_id: String?
    let language_machine_name: String?
    let language_label: String?
    let active: String?
    let events: eventData?
}

struct eventData : Decodable {
    
    let invited_events : [invitedEventsData]?
    let created_events : [createdEventsData]?
    
    
}

struct invitedEventsData : Decodable {
    let event_id : String?
    let target_type_id : String?
    let target_id : String?
    let creator_id : String?
    let first_name : String?
    let last_name : String?
    let other_name : String?
    let profile_pic : String?
    let profile_pic_filename : String?
    let profile_folder_name : String?
    let event_type_id : String?
    let name: String?
    let about: String?
    let cover_pic : String?
    let cover_pic_filename : String?
    let cover_folder_name : String?
    let place_id : String?
    let place_name : String?
    let start_datetime : String?
    let end_datetime : String?
    let event_key_words : String?
    let is_private : String?
    let active : String?
}

struct createdEventsData : Decodable {
    let event_id : String?
    let target_type_id : String?
    let target_id : String?
    let creator_id : String?
    let first_name : String?
    let last_name : String?
    let other_name : String?
    let profile_pic : String?
    let profile_pic_filename : String?
    let profile_folder_name : String?
    let event_type_id : String?
    let name: String?
    let about: String?
    let cover_pic : String?
    let cover_pic_filename : String?
    let cover_folder_name : String?
    let place_id : String?
    let place_name : String?
    let start_datetime : String?
    let end_datetime : String?
    let event_key_words : String?
    let event_ticket_url : String?
    let is_private : String?
    let active : String?
}
