//
//  profileViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 14/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
extension profileViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
class profileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var timelineCoverPic: UIImageView!
    
    @IBOutlet weak var timelineProfilePictureTop: UIImageView!
    
    @IBOutlet weak var timelineUsernameTop: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var addFriend: UIButton!
    
    
    @IBAction func addFriend(_ sender: UIButton) {
        
        sendRequest()
        sender.isEnabled = false
        
    }
    
    var postsArray = [postData]()
    var comments : Int?
    var likes : Int?
    var friendId = 421
    var searchedUserId : String?
    var searchedUser : userResults?
    //var userArray = userData()
    override func viewDidLoad() {
        super.viewDidLoad()
        //fetch posts!
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        //get user being searched for's userID
        let wantedUser = searchedUser?.user_id
        
        if userId == wantedUser {
            
            addFriend.isEnabled = false
            
        }else{
            
        }
        
        DispatchQueue.main.async(execute: {
            print(self.searchedUser?.user_id!)
            self.loadPosts()
            self.getUser()
        })
        
        tableView.separatorStyle = .none
        
        // Do any additional setup after loading the view.
        
  
        //make profile picture edges round.
        timelineProfilePictureTop.layer.cornerRadius = timelineProfilePictureTop.bounds.width / 20
        timelineProfilePictureTop.clipsToBounds = true
        
        //dismiss keyboard when u tap anywhere
        self.hideKeyboardWhenTappedAround() 
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //function to send friend request
    
    func sendRequest() {
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/user/friend-request")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "sender_id=\(userId)&recipient_id=\(friendId)&action=\("send")"
        
        print(body)
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            
                            // get main queue to communicate back to user
                            DispatchQueue.main.async(execute: {
                                let message = "Friend request sent"
                                
                                appDelegate.infoView(message: message, color: colorBrandBlue)
                             
                                
                            })
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            print(message)
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        print(message)
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
    }
    
    
    //function to get user information
    func getUser() {
        print("user fetched!")
        let wantedUser = searchedUser?.user_id
        
        let userNum = wantedUser
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/user/single-user?user_id=\(userNum!)")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userInfo = try JSONDecoder().decode(user.self, from: data)

                
                let usersArray = userInfo.data
                //iterate through array to get specific values from struct
                DispatchQueue.main.async(execute: {
                    self.timelineUsernameTop.text = usersArray.fullname
                    
                    //set cover profile pic
                    let coverPicture = usersArray.cover_pic_filename
                    
                    if coverPicture != nil {
                        //load profile picture from library
                        let urlString = "https://deiplaces.com/uploads/cover-picture/image_1600/"+(coverPicture)!
                        let profileURL = URL(string: urlString)
                        
                        self.timelineCoverPic?.downloadedFrom(url: profileURL!)
                        
                    }else{
                        
                        print("you have no coverPic picture set")
                        
                    }
                    
                    //set profile picture
                    let profilePicture = usersArray.profile_pic_filename
                    
                    if profilePicture != nil {
                        //load profile picture from library
                        let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_400/"+(profilePicture)!
                        let profileURL = URL(string: urlString)
                        
                        self.timelineProfilePictureTop.downloadedFrom(url: profileURL!)
                        
                    }else{
                        
                        print("you have no coverPic picture set")
                        
                    }
                })
                
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
        
    }
    
    
    //function to fetch posts from the internet and load them!
    func loadPosts(){
        print("posts triggered")
        let wantedUser = searchedUser?.user_id
        
        let userNum = wantedUser
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/posts/target_posts?target_type_id=1&target_id=\(userNum!)&start=1&rows=10")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userPosts = try JSONDecoder().decode(deiPost.self, from: data)
                
     
                self.postsArray = userPosts.data
                //iterate through array to get specific values from struct
                
                for postArray in self.postsArray{
                    //print(postArray.content)
                    print(postArray)
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                 
                }
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if section == 0{
            return 1
        }else{
            return postsArray.count
        }
        
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileViewStatusCell", for: indexPath) as! profileViewStatusTableViewCell
            
            profilePicture = UserDefaults.standard.object(forKey: "userPic") as? String

            
            if profilePicture != nil {
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_400/"+(profilePicture)!
                let profileURL = URL(string: urlString)
                
                cell.timelineProfilePic?.downloadedFrom(url: profileURL!)
                //timelineProfilePictureTop?.downloadedFrom(url: profileURL!)
                
            }else{
                
                print("you have no profile picture set")
                
            }
            return cell
        }else{
            
            if postsArray[indexPath.row].photos != nil{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileViewPostCell", for: indexPath) as! profileViewPostTableViewCell
            
                
                if postsArray[indexPath.row].comments != nil {
                    
                    comments = postsArray[indexPath.row].comments?.count
                }else {
                    
                    comments = 0
                }
                
                if postsArray[indexPath.row].like_list != nil {
                    
                    likes = postsArray[indexPath.row].like_list?.count
                    
                }else{
                    
                    likes = 0
                }
                
            cell.timelineUsername.text = postsArray[indexPath.row].fullname
            //timelineUsernameTop.text = postsArray[indexPath.row].fullname
            cell.timelineHours.text = postsArray[indexPath.row].data_created
            cell.timelineCaption.text = postsArray[indexPath.row].content
            //cell.timeAgoLabel.text = postsArray[indexPath.row].modified
            //load profile picture from library
            let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_400/"+(postsArray[indexPath.row].profile_pic_filename)!
            let profileURL = URL(string: urlString)
            
            cell.timelinePostsProfPic.downloadedFrom(url: profileURL!)
            
                
                //15 Likes     30 Comments     500 Shares
                cell.profileStats.text = "\(likes!) Likes      \(comments!)  Comments"
                
                
            //iterate through posts images images array
            
            //load post picture from server library
            var postImageName : String?
            if postsArray[indexPath.row].photos != nil{
                let postImage = postsArray[indexPath.row].photos
                for postsImage in postImage!{
                    
                    postImageName = postsImage.filename!
                    
                }
                let urlPostImageString = "https://deiplaces.com/uploads/post-picture/image_500/"+(postImageName)!
                let postsImageUrl = URL(string: urlPostImageString)
                cell.timelinePicture.downloadedFrom(url: postsImageUrl!)
            }else{
                print("Post has no picture")
            }
           
            return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoImageProfileCell", for: indexPath) as! NoImageProfilePostTableViewCell
                
                
                if postsArray[indexPath.row].comments != nil {
                    
                    comments = postsArray[indexPath.row].comments?.count
                }else {
                    
                    comments = 0
                }
                
                if postsArray[indexPath.row].like_list != nil {
                    
                    likes = postsArray[indexPath.row].like_list?.count
                    
                }else{
                    
                    likes = 0
                }
                
                cell.noImageUsername.text = postsArray[indexPath.row].fullname
                cell.noImageTime.text = postsArray[indexPath.row].data_created
                cell.noimageCaption.text = postsArray[indexPath.row].content
                
                
                //15 Likes     30 Comments     500 Shares
                cell.noImageStats.text = "\(likes!) Likes      \(comments!)  Comments"
                
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/thumb_400/"+(postsArray[indexPath.row].profile_pic_filename)!
                let profileURL = URL(string: urlString)
                
                cell.noImageProfPic.downloadedFrom(url: profileURL!)
                return cell
            }
        }
        
    }
    

}
