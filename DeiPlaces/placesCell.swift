//
//  placesCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 27/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

protocol placesCellDelegate {
    func didTapMore(id: String)
}

class placesCell : UITableViewCell
{
 
    @IBOutlet weak var placesProfilePic: UIImageView!
    
    @IBOutlet weak var placesTitle: UILabel!
    
    @IBOutlet weak var placesCaption: UILabel!
    
    @IBOutlet weak var placeRating: UILabel!
    
    @IBOutlet weak var placesLikes: UILabel!
    
    @IBOutlet weak var postID: UILabel!
    
    var delegate: placesCellDelegate?
    
    @IBAction func moreTapped(_ sender: Any) {
        
        delegate?.didTapMore(id: postID.text!)
        
    }
    
    
}
