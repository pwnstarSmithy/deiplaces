//
//  CommentNoImagePostTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 15/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

protocol commentNoImageCellDelegate {
   func didTapMore(id: String)
    func didLikeNoImage(id: String)
}

class CommentNoImagePostTableViewCell: UITableViewCell {

    @IBOutlet weak var noImageProfilePic: UIImageView!
    
    @IBOutlet weak var noImageUsername: UILabel!
    
    @IBOutlet weak var noImageTime: UILabel!
    
    @IBOutlet weak var noImagePost: UILabel!
    
    @IBOutlet weak var noImageLikeAndComment: UILabel!

    @IBAction func likePressed(_ sender: Any) {
        
         delegate?.didLikeNoImage(id: postID.text!)
    }
    
    
    @IBOutlet weak var postID: UILabel!
    
    var delegate: commentNoImageCellDelegate?
    
    @IBAction func moreTapped(_ sender: Any) {
        delegate?.didTapMore(id: postID.text!)
        
    }
    
    
}
