//
//  ViewCommentTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 15/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class ViewCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var showComments: UILabel!
    
    @IBOutlet weak var commentProfilePic: UIImageView!
    
    @IBOutlet weak var commentUsername: UILabel!
    
}
