//
//  invitedEventsCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 01/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

protocol invitedEventsDelegate {
    func didTapMore(id: String)
}

class InvitedEventsCell : UITableViewCell
{
    
    @IBOutlet weak var invitedImage: UIImageView!
    
    @IBOutlet weak var invitedName: UILabel!
    
    @IBOutlet weak var invitedEventName: UILabel!
    
    @IBOutlet weak var invitedStartDate: UILabel!
    
    @IBOutlet weak var invitedEndDate: UILabel!
    
    @IBOutlet weak var invitedAbout: UILabel!
    
    
    @IBOutlet weak var postID: UILabel!
    
    var delegate: invitedEventsDelegate?
    @IBAction func moreTapped(_ sender: Any) {
        
        delegate?.didTapMore(id: postID.text!)
    }
    
}
