//
//  AddCommentTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 15/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class AddCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var commentNow: UITextView!
    
    @IBOutlet weak var sendButton: UIButton!
    
}

