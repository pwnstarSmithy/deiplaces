//
//  search.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 07/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

struct search: Decodable {
    
    let status: String
    let message: String
    let data: searchResults
    
}

struct searchResults: Decodable {
    
    let user_results: [userResults]?
    let place_results: [placeResults]?
    let post_results: [postResults]?
    let event_results: [eventResults]?
    let search_results: String?
}

struct userResults: Decodable {
    let user_id: String?
    let first_name: String?
    let last_name: String?
    let other_name: String?
    let fullname: String?
    let alias: String?
    let about: String?
    let sex_id: String?
    let sex: String?
    let birth_date: String?
    let relation_status_id: Int?
    let relation_status: String?
    let relation_user_id: Int?
    let relation_user_first_name: String?
    let relation_user_last_name: String?
    let relation_user_fullname: String?
    let location: String?
    //let contact: String?
    let profile_pic: String?
    let profile_folder_name: String?
    let profile_pic_filename: String?
    let cover_pic: String?
    let cover_folder_name: String?
    let cover_pic_filename: String?
//    let followers_list : followersList?
//    let following_list : followingList?
//    let mutual_list : mutualList?
    let request_counter: String?
    let dream_destination_list: Int?
    //let hidden_content_list: Int?
    let email: String?
    let username: String?
    let password: String?
    let salt: String?
    let social_media_auth: String?
    let social_media_id: String?
    let date_created: String?
    let last_seen: String?
    let is_suspended: String?
    let is_notified: String?
    let approve_token: String?
    let device_token: String?
    //let language_id: Int?
    let language_machine_name: String?
    let language_label: String?
    let active: String?
}
struct placeResults: Decodable {
    let place_id: String?
    let name: String?
    let username: String?
    let place_type_id: String?
    let place_type_label: String?
    let place_category_id: String?
    let place_category_label: String?
    let label: String?
    let color_scheme: String?
    let address: String?
    let latitude: String?
    let longitude: String?
    let geoname_id: String?
    let about: String?
    //let contact: String?
    let open_hours_list: String?
    let profile_pic: String?
    let profile_pic_filename: String?
    let cover_pic_filename: String?
    let cover_folder_name: String?
    let feature_list: String?
//    let like_list: [likeData]?
    let visited_list: String?
    let date_created: String?
    let is_flagged: String?
    let flag_history: String?
    let is_official: String?
    let is_published: String?
    let is_viewable: String?
    let active: String?
//    let rating: ratingsData?
}
struct postResults: Decodable {
    let post_id: String?
    let target_type_id: String?
    let target_id: String?
    let creator_id: String?
    let fullname: String?
    let first_name: String?
    let last_name: String?
    let other_name: String?
    let profile_pic: String?
    let profile_pic_filename: String?
    let post_type_id: String?
    let feeling_id: String?
    let feeling_name: String?
    let content: String?
    //let tag_list: String?
//    let like_list: [likeList]?
    //let view_list: String?
    let data_created: String?
    let is_share: String?
    let orig_post_id: String?
    let post_as_target: String?
    let modified: String?
    let permission_list: String?
    let reported: String?
    let active: String?
//    let photos: [photoArray]?
    //let videos: String?
//    let comments: [comments]?
}
struct eventResults: Decodable {
    let event_id : String?
    let target_type_id : String?
    let target_id : String?
    let creator_id : String?
    let first_name : String?
    let last_name : String?
    let other_name : String?
    let profile_pic : String?
    let profile_pic_filename : String?
    let profile_folder_name : String?
    let event_type_id : String?
    let name: String?
    let about: String?
    let cover_pic : String?
    let cover_pic_filename : String?
    let cover_folder_name : String?
    let place_id : String?
    let place_name : String?
    let start_datetime : String?
    let end_datetime : String?
    let event_key_words : String?
    let is_private : String?
    let active : String?
}
