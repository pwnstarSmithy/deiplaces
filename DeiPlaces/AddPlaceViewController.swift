//
//  AddPlaceViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 09/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class AddPlaceViewController: UIViewController, CLLocationManagerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    var placeTypeArray = [placesType]()
    var placeTypeTuple:[(name: String, code: String)] = []
    var placeTypeSelected : String?
    
    @IBOutlet weak var nameOfPlace: UITextField!
    
    @IBOutlet weak var aboutPlace: UITextView!
    
    @IBOutlet weak var addressOfPlace: UITextField!
    
    @IBOutlet weak var emailOfPlace: UITextField!
    
    @IBOutlet weak var phoneOfPlace: UITextField!
    
    @IBOutlet weak var websiteOfPlace: UITextField!
    
    @IBOutlet weak var featuresOfPlace: UITextField!
    
    //@IBOutlet weak var openHoursOfPlace: UITextField!
    
    @IBOutlet weak var placeTypeField: UITextField!
    
    @IBOutlet weak var openingHour: UITextField!
    
    @IBOutlet weak var closingHour: UITextField!
    
    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    
    var latitude : Double?
    var longitude : Double?
    
    var name : String?
    var about : String?
    var address : String?
    var email : String?
    var phone : String?
    var website : String?
    var privacy : String?
    var features : String?
    var openHours : String?
    var placeType : String?
    
    let timeOpen = ["Monday - Saturday", "Monday - Sunday"]
    var pickerView = UIPickerView()
    let picker = UIDatePicker()
    let closePicker = UIDatePicker()
    @IBAction func isSubmitted(_ sender: Any) {
        
        createPlace()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        loadPlaceTypes()
        //assign published to true
        privacy = "1"
    
        
        //location assignments
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        createDatePicker()
        createClosePicker()
        //set pickerview
        // Do any additional setup after loading the view.
        pickerView.delegate = self
        pickerView.dataSource = self
        placeTypeField.inputView = pickerView
        
    }
    
    func createClosePicker(){
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        // done button for tool bar
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClosePressed))
        toolbar.setItems([done], animated: false)
        
        closingHour.inputAccessoryView = toolbar
        closingHour.inputView = closePicker
        
        //set date picker to show only date not time
        closePicker.datePickerMode = .time
    }
    
    @objc func doneClosePressed(){
        
        //format date
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .long
        let dateString = formatter.string(from: closePicker.date)
        
        closingHour.text = "\(dateString)"
        self.view.endEditing(true)
        
    }
    
    
    func createDatePicker(){
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        // done button for tool bar
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        
        openingHour.inputAccessoryView = toolbar
        openingHour.inputView = picker
        
        //set date picker to show only date not time
        picker.datePickerMode = .time
        
        
    }
    @objc func donePressed(){
        
        //format date
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .long
        let dateString = formatter.string(from: picker.date)
        
        openingHour.text = "\(dateString)"
        self.view.endEditing(true)
        
    }
    // returns the number of 'columns' to display.
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
        
    }
    
    
    // returns the # of rows in each component..
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return placeTypeTuple.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return placeTypeTuple[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        placeTypeSelected = placeTypeTuple[row].code
        
        placeTypeField.text = placeTypeTuple[row].name
        
        placeTypeField.resignFirstResponder()
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations[0]
        
        let center = location.coordinate
        
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        
        let region = MKCoordinateRegionMake(center, span)
        
        mapView.setRegion(region, animated: true)
        mapView.showsUserLocation = true
        
        longitude = location.coordinate.longitude
        latitude = location.coordinate.latitude
        //print(location.coordinate)
    }
    
    func loadPlaceTypes(){
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/places/place-types")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let placePosts = try JSONDecoder().decode(userPlaceType.self, from: data)
                
                //print(userPosts.data)
                //print("it works")
                
                self.placeTypeArray = placePosts.data
                //iterate through array to get specific values from struct
                
                
                for placeArray in self.placeTypeArray{
                    
                    let placePickerId = placeArray.place_type_id
                    let placePickerName = placeArray.label
                    
                    self.placeTypeTuple.append((name: placePickerName! , code: placePickerId!))
                    
                    DispatchQueue.main.async {
                        self.pickerView.reloadAllComponents()
                    }
                    
                }
                
                
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
    }
    
    func createPlace() {
        
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
         //variables to be passed to server
        if nameOfPlace.text != nil {
            name = nameOfPlace.text!
            
        }else{
            print("name has no values")
        }
        
        if aboutPlace.text != nil{
            about = aboutPlace.text
            
        }else{}
        if addressOfPlace.text != nil{
            address = addressOfPlace.text!
            
        }else{}
        if emailOfPlace.text != nil{
            email = emailOfPlace.text!
            
        }else{}
        if phoneOfPlace.text != nil {
            phone = phoneOfPlace.text!
            
        }else{}
        if websiteOfPlace.text != nil {
            website = websiteOfPlace.text!
            
        }else{}
        if featuresOfPlace.text != nil {
            features = featuresOfPlace.text!
            
        }else{}
        if openingHour.text != nil {
            openHours = openingHour.text!
            
        }else{}
        if closingHour.text != nil {
            openHours = closingHour.text!
            
        }else{}
        if placeTypeField.text != nil {
            placeType = placeTypeField.text!
            
        }else{}
  
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/places/add-place")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        
        
        let body = "user_id=\(userId)&name=\(name!)&about=\(about!)&address=\(address!)&email=\(email!)&phone=\(phone!)&website=\(website!)&is_published=\(privacy!)&openHours=\(openHours!)&latitude=\(latitude!)&longitude=\(longitude!)&place_type=\(placeTypeSelected!)"
       
        print(body)
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                    
                        
                        // if there is some message - post is made
                        if message != nil {
                            print(message!)
                       //Do stuff if succes
                            
                            // get main queue to communicate back to user
                            DispatchQueue.main.async(execute: {
                                let message = "Place successfully added!"
                                
                                self.aboutPlace.text = ""
                                self.addressOfPlace.text = ""
                                self.nameOfPlace.text = ""
                                self.emailOfPlace.text = ""
                                self.phoneOfPlace.text = ""
                                self.websiteOfPlace.text = ""
                                self.featuresOfPlace.text = ""
                                //self.openHoursOfPlace.text = ""
                                self.openingHour.text = ""
                                self.closingHour.text = ""
                                self.placeTypeField.text = ""
                                
                                appDelegate.infoView(message: message, color: colorBrandBlue)
                            })
                        
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            print(message)
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        print(message)
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
        }.resume()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //hide keyboard
        self.view.endEditing(false)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
