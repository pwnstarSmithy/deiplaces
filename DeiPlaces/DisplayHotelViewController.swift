//
//  DisplayHotelViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 29/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class DisplayHotelViewController: UIViewController {

    @IBOutlet weak var hotelCover: UIImageView!
    
    @IBOutlet weak var hotelProfilePic: UIImageView!
    
    @IBOutlet weak var hotelName: UILabel!
    
    @IBOutlet weak var AboutHotel: UILabel!
    
    @IBOutlet weak var hotelLocation: UILabel!
    
    @IBOutlet weak var hotelOpenHours: UILabel!
    
    
    var hotelArray = [hotelResults]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        for hotel in hotelArray {

            hotelName.text = hotel.name
            AboutHotel.text = hotel.about
            hotelLocation.text = hotel.address

        }

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
