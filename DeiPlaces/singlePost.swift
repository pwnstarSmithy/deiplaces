//
//  singlePost.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 20/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
struct singlePost: Decodable {
    
    let status: String
    let message: String
    let data: singlePostData
    
}

struct singlePostData: Decodable {
    
    let post_id: String?
    let target_type_id: String?
    let target_id: String?
    let creator_id: String?
    let fullname: String?
    let first_name: String?
    let last_name: String?
    let other_name: String?
    let profile_pic: String?
    let profile_pic_filename: String?
    let post_type_id: String?
    let feeling_id: String?
    let feeling_name: String?
    let content: String?
    //let tag_list: String?
    let like_list: [singleLikeList]?
    //let view_list: String?
    let data_created: String?
    let is_share: String?
    let orig_post_id: String?
    let post_as_target: String?
    let modified: String?
    let permission_list: String?
    let reported: String?
    let active: String?
    let photos: [singlePhoto]?
    //let videos: String?
    let comments: [singleComment]?
}
struct singleLikeList: Decodable {
    let user_id: String?
    let name: String?
    let date_time: String?
    let reaction: String?
}

struct singleComment : Decodable {
    let comment_id: String?
    let target_type_id: String?
    let target_id: String?
    let creator_id: String?
    let fullname: String?
    let first_name: String?
    let last_name: String?
    let other_name: String?
    let profile_pic: String?
    let profile_pic_filename: String?
    let content: String?
    let like_list: String?
    let date_created: String?
    let active: String?
}

struct singlePhoto : Decodable {
    let photo_id: String?
    let target_type_id: String?
    let target_id: String?
    let creator_id: String?
    let first_name: String?
    let last_name: String?
    let other_name: String?
    let profile_pic_filename: String?
    let profile_folder_name: String?
    let media_path_id: String?
    let folder_name: String?
    let filename: String?
    let like_list: String?
    let date_created: String?
    let active: String?
}

