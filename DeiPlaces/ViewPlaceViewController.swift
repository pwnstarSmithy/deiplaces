//
//  ViewPlaceViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 22/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class ViewPlaceViewController: UIViewController {

    
    @IBOutlet weak var coverPic: UIImageView!
    
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBAction func changeCover(_ sender: Any) {
    }
    
    @IBAction func changeProfile(_ sender: Any) {
    }
    
    @IBOutlet weak var placeName: UILabel!
    
    
    @IBOutlet weak var location: UILabel!
    
    
    @IBOutlet weak var phone: UILabel!
    
    
    @IBOutlet weak var website: UILabel!
    
    @IBOutlet weak var about: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
