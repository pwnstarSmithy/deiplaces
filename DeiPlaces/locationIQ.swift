//
//  locationIQ.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 25/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

struct location : Codable {
    let place_id : String?
    let licence : String?
    let osm_type : String?
    let osm_id : String?
    let boundingbox : [String]?
    let lat : String?
    let lon : String?
    let display_name : String?
//    let class : String?
    let type : String?
    let importance : Double?
    let icon : String?
}
