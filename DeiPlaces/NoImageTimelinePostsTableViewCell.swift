//
//  NoImageTimelinePostsTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 23/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class NoImageTimelinePostsTableViewCell: UITableViewCell {

    @IBOutlet weak var noImageProfPic: UIImageView!
    
    @IBOutlet weak var noImageUsername: UILabel!
    
    @IBOutlet weak var noimageTime: UILabel!
    
    @IBOutlet weak var noImageCaption: UILabel!
    
    @IBOutlet weak var noImageStats: UILabel!
    
    @IBOutlet weak var noImageGlobe: UIImageView!
    
    @IBOutlet weak var postID: UILabel!
    
    
}
