//
//  RegisterViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 20/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit
 //var genderInt : Int?

var genderInt : String?
class RegisterViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
      var loginActivity:UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var dateField: UITextField!
    
    @IBOutlet weak var genderField: UITextField!
    
   
    
    @IBOutlet weak var firstName: UITextField!
    
    @IBOutlet weak var LastName: UITextField!
    
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var confirmPassword: UITextField!
    
    var tokenVar : String?
    
    var dateFieldVar : String?
    var genderVar : String?
    //@IBOutlet weak var signupPressed: UIButton!
    
    @IBAction func signupPressed(_ sender: Any) {
        
        firstName.resignFirstResponder()
        LastName.resignFirstResponder()
        userName.resignFirstResponder()
        email.resignFirstResponder()
        password.resignFirstResponder()
        confirmPassword.resignFirstResponder()
        
        if firstName.text!.isEmpty || LastName.text!.isEmpty || userName.text!.isEmpty || email.text!.isEmpty || password.text!.isEmpty || confirmPassword.text!.isEmpty {
            
            //Insert warning placeholders
            firstName.attributedPlaceholder = NSAttributedString(string: "Please Enter First Name", attributes: [NSAttributedStringKey.foregroundColor:UIColor.blue])
                 LastName.attributedPlaceholder = NSAttributedString(string: "Please Enter Last Name", attributes: [NSAttributedStringKey.foregroundColor:UIColor.blue])
            
                userName.attributedPlaceholder = NSAttributedString(string: "Please Enter Username", attributes: [NSAttributedStringKey.foregroundColor:UIColor.blue])
            
                email.attributedPlaceholder = NSAttributedString(string: "Please enter email address", attributes: [NSAttributedStringKey.foregroundColor:UIColor.blue])
            
                password.attributedPlaceholder = NSAttributedString(string: "Please enter password", attributes: [NSAttributedStringKey.foregroundColor:UIColor.blue])
            
        }else{
            // start activity indicator
            startAnimating()
            
            // assign textfields to variables
            let firstNameVar = firstName.text!
            let lastNameVar = LastName.text!
            let usernameVar = userName.text!
            let emailVar = email.text!
            let passwordVar = password.text!
            let confirmPasswordVar = confirmPassword.text!
            
            if dateField.text!.isEmpty{
                dateFieldVar = ""
            }else{
                dateFieldVar = dateField.text!
            }
            
            if genderInt != nil {
                 genderVar = genderInt!
            }else{
                genderVar = ""
            }
         
            
           
            //dummy token to test registration!
            if firebaseToken != nil {
                tokenVar = firebaseToken
            }else{
                
            }
            
            //send request to server
            guard let registerUrl = URL(string: "https://deiplaces.com/api/index.php/Auth/register") else {return}
            
            //request url
            var request = URLRequest(url: registerUrl)
            
            // method to pass data
            request.httpMethod = "POST"
            
            let body = "username=\(usernameVar)&password=\(passwordVar)&confirm_password=\(confirmPasswordVar)&first_name=\(firstNameVar)&last_name=\(lastNameVar)&email=\(emailVar)&birth_date=\(dateFieldVar!)&sex_id=\(genderVar!)&token=\(tokenVar!)"
            print(body)
              request.httpBody = body.data(using: String.Encoding.utf8)
            
            //launch session
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: request) { (data, response, error) in
                
                guard let data = data else {return}
                print(data)
                do {
                    
                    //let json = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    //print(json)
                    
                    let userDetails = try JSONDecoder().decode(user.self, from: data)
                    
                    //print(userDetails.data.fullname)
                    
                    let statusMessage = userDetails.status
                    
                    if statusMessage == "true"{
                        print(userDetails.data)
                        // go to tabbar / home page
                        DispatchQueue.main.async(execute: {
                            //function to stop activity indicator
                            self.stopAnimating()
                            
                            let successAlert = UIAlertController(title: "Registration Successful", message: "Please check your email for confirmation", preferredStyle: UIAlertControllerStyle.alert)
                            
                            successAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                                
                                successAlert.dismiss(animated: true, completion: nil)
                                 }))
                             self.present(successAlert, animated: true, completion: nil)
                            
                            appDelegate.loginRedirectToLoginPage()
                        })
                        
                        
                    }else{
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            
                            //function to stop activity indicator
                            self.stopAnimating()
                            
                            let message = userDetails.message
                            
                            let failAlert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            
                            failAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                                
                                failAlert.dismiss(animated: true, completion: nil)
                                
                                
                            }))
                            
                            DispatchQueue.main.async(execute: {
                                self.present(failAlert, animated: true, completion: nil)
                            })
                            
                        })
                        
                        
                        
                    }
                    
                    
                }catch{
                    
                    
                    DispatchQueue.main.async(execute: {
                        // get main queue to communicate back to user
                        self.stopAnimating()
                        
                        let message = "\(error)"
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    print(error)
                    return
       
                }
                
                
                
            }
            task.resume()
        }
        
        
    }
    
    let genders = ["Male", "Female"]
    //variable of array of named tuples
    var genderInfo:[(name: String, code: String)] = []
    
    var pickerView = UIPickerView()
    
    let picker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createDatePicker()

        // Do any additional setup after loading the view.
        pickerView.delegate = self
        pickerView.dataSource = self
        genderField.inputView = pickerView
    }
    
    func createDatePicker(){
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        // done button for tool bar
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        
        dateField.inputAccessoryView = toolbar
        dateField.inputView = picker
        
        //set date picker to show only date not time
        picker.datePickerMode = .date
        
        
    }
    
    @objc func donePressed(){
        
        //format date
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        let dateString = formatter.string(from: picker.date)
        
        dateField.text = "\(dateString)"
        self.view.endEditing(true)
        
    }
    
    // returns the number of 'columns' to display.
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
        
    }
    
    
    // returns the # of rows in each component..
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return genders.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderField.text = genders[row]
        
      
        if genderField.text == "Male"{
            
            genderInt = "1"
            
        }else{
            
            genderInt = "2"
            
        }
        
        
        genderField.resignFirstResponder()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startAnimating() {
        loginActivity.center = self.view.center
        
        loginActivity.hidesWhenStopped = true
        loginActivity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(loginActivity)
        
        loginActivity.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        loginActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //hide keyboard
        self.view.endEditing(false)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
