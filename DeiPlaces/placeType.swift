//
//  placeType.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 10/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//
import Foundation
struct userPlaceType : Decodable {
    let status: String
    let message: String
    let data: [placesType]
}
struct placesType : Decodable {
    let place_type_id : String?
    let place_category_id : String?
    let machine_name : String?
    let label : String?
    let color_scheme : String?
    let description : String?
    let active : String?
}

