//
//  HotelTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 29/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class HotelTableViewCell: UITableViewCell {

    
    @IBOutlet weak var hotelProfPic: UIImageView!
    
    
    @IBOutlet weak var hotelName: UILabel!
    
}
