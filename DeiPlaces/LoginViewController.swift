//
//  LoginViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 20/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    var loginActivity:UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBAction func loginPressed(_ sender: Any) {
        
        //close keyboard once button tapped
        username.resignFirstResponder()
        password.resignFirstResponder()
        
        
        if username.text!.isEmpty || password.text!.isEmpty {
            
            //Insert warning placeholders
            username.attributedPlaceholder = NSAttributedString(string: "Please Enter Username", attributes: [NSAttributedStringKey.foregroundColor:UIColor.blue])
            password.attributedPlaceholder = NSAttributedString(string: "Please Enter Password", attributes: [NSAttributedStringKey.foregroundColor:UIColor.blue])
        }else{
            
            // start activity indicator
            startAnimating()
            
            //assign values to textfields
            let usernameVar = username.text!
            let passwordVar = password.text!
            
            
            //send request to server
            let url = NSURL(string: "https://deiplaces.com/api/index.php/Auth/login")!
            
            //request url
            var request = URLRequest(url: url as URL)
            
            // method to pass data
            request.httpMethod = "POST"
            let body = "username=\(usernameVar)&password=\(passwordVar)"
            
            request.httpBody = body.data(using: String.Encoding.utf8)
            //launch session
            URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data else{return}
                // check if no error
                if error == nil{
                    
                    do{
                        
                        //let json = try JSONSerialization.jsonObject(with: data, options: [])
                        
                        //print(json)
                        
                        let userDetails = try JSONDecoder().decode(user.self, from: data)
                        
                        //print(userDetails.data.fullname)
                     let statusMessage = userDetails.status
                        
                        if statusMessage == "true"{
                            
                            //assign values we need to save on users phone
                            let userData = userDetails.data
                            let userId = userData.user_id
                            //save userid to phone
                            UserDefaults.standard.set(userId, forKey:"userId")
                            userID = UserDefaults.standard.value(forKey: "userId") as? String
                            
                            
                            if userData.profile_pic_filename != nil {
                            let userPic = userData.profile_pic_filename
                                
                                //save user profile picture to phone
                                UserDefaults.standard.set(userPic, forKey:"userPic")
                                profilePicture = UserDefaults.standard.value(forKey: "userPic") as? String
                                
                                
                                
                                }else{
                               print("no picture saved")
                            }
                            
                            
                            if userData.cover_pic_filename != nil {
                                
                                let userCoverPic = userData.cover_pic_filename
                                
                                //save cover Pic on user phone
                                UserDefaults.standard.set(userCoverPic, forKey:"userCoverPic")
                                coverPicture = UserDefaults.standard.value(forKey: "userCoverPic") as? String
                                
                            }else{
                                
                                print("No cover Profile pic")
                                
                            }
                            
                            // go to tabbar / home page
                            DispatchQueue.main.async(execute: {
                                //function to stop activity indicator
                                self.stopAnimating()
                                
                                appDelegate.login()
                            })
                    
                            
                            
                            
                        }else{
                            
                            // get main queue to communicate back to user
                            DispatchQueue.main.async(execute: {
                                
                                //function to stop activity indicator
                                self.stopAnimating()
                                
                                let message = userDetails.message
                                
                                let failAlert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                
                                failAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                                    
                                    failAlert.dismiss(animated: true, completion: nil)
                                    
                                    
                                }))
                                
                                DispatchQueue.main.async(execute: {
                                    self.present(failAlert, animated: true, completion: nil)
                                })
                                
                            })
                            
                            
                            
                        }
                        
                        
                    }catch {
                        
                       
                        DispatchQueue.main.async(execute: {
                            // start activity indicator
                            self.stopAnimating()
                            
                            //let message = "\(error)"
                            appDelegate.infoView(message: "Check your login details", color: colorSmoothRed)
                            print(error)
                        })
                        return
                    }
                    
                }else{
                    
                    // start activity indicator
                    self.stopAnimating()
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                }
                
                
                }.resume()
            
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func startAnimating() {
        loginActivity.center = self.view.center
        
        loginActivity.hidesWhenStopped = true
        loginActivity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(loginActivity)
        
        loginActivity.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        loginActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //hide keyboard
        self.view.endEditing(false)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
