//
//  FriendRequestTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 18/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

protocol FriendRequestDelegate {
    
    func didAccept(id: String)
    func didDecline(id: String)
    
}

class FriendRequestTableViewCell: UITableViewCell {

    
    @IBOutlet weak var FriendRequestProfPic: UIImageView!
    
    
    @IBOutlet weak var friendRequestName: UILabel!
    
    
    @IBOutlet weak var receipientID: UILabel!
    
    var friendId: UILabel!
    
    var delegate: FriendRequestDelegate?
    
    @IBAction func addFriend(_ sender: Any) {
        
        delegate?.didAccept(id: receipientID.text!)
        
    }
    
    @IBAction func declineFriend(_ sender: Any) {
        
        delegate?.didDecline(id: receipientID.text!)
        
    }
    
    
}
