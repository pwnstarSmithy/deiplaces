//
//  hotels.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 29/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation


struct hotels: Decodable {
    
    let status: String
    let message: String
    let data: [hotelResults]
    
}

struct hotelResults: Decodable {
    let place_id : String?
    let name : String?
    let username : String?
    let place_type_id : String?
    let place_type_label : String?
    let label : String?
    let color_scheme : String?
    let address : String?
    let latitude: String?
    let longitude: String?
    let geoname_id: String?
    let about: String?
    let contact: contacter?
//    let open_hours_list : String?
    let profile_pic: String?
    let profile_pic_filename: String?
    let profile_folder_name: String?
    let cover_pic: String?
    let cover_pic_filename: String?
    let cover_folder_name: String?
    let feature_list: [features]?
    let like_list: String?
    let invite_list: String?
    let visited_list: String?
    let date_created: String?
    let is_flagged: String?
    let flag_history: String?
    let is_official: String?
    let is_published: String?
    let is_viewable: String?
    let active: String?
    let rating: ratings
    
    
    
}
struct features: Decodable {
    let feature_id: String?
    let machine_name: String?
    let label: String?
    let description: String?
    let icon: String?
    let active: String?
}
struct contacter: Decodable {
    let email: String?
    let phone: String?
    let website: String?
}

struct ratings: Decodable{
    let average_score : Int
    let reviewers : Int
}
