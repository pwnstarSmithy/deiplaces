//
//  statusCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 30/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class statusCell : UITableViewCell
{
    
    @IBOutlet weak var statusProfilePic: UIImageView?
    @IBOutlet weak var statusUpdate: UILabel!
    
    @IBOutlet weak var createPost: UIButton!
    
    
}
