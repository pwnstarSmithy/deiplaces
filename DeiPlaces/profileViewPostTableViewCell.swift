//
//  profileViewPostTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 14/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class profileViewPostTableViewCell: UITableViewCell {

    @IBOutlet weak var timelinePostsProfPic: UIImageView!
    
    @IBOutlet weak var timelineUsername: UILabel!
    
    @IBOutlet weak var timelineHours: UILabel!
    
    @IBOutlet weak var timelineCaption: UILabel!
    
    @IBOutlet weak var timelinePicture: UIImageView!
    
    @IBOutlet weak var globeImageTimeline: UIImageView!

    @IBOutlet weak var profileStats: UILabel!
    
    @IBOutlet weak var postID: UILabel!
    
    
    @IBAction func moreTapped(_ sender: Any) {
        
        
    }
    
    
}
