//
//  eventType.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 03/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation
struct userEventType : Decodable {
    let status: String
    let message: String
    let data: [eventsType]
}
struct eventsType : Decodable {
    let event_type_id : String?
    let machine_name : String?
    let label : String?
    let color_scheme : String?
    let description : String?
    let active : String?
}
