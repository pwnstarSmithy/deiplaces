//
//  AddEventViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 03/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class AddEventViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var eventTypeArray = [eventsType]()
    var eventTypeTuple:[(name: String, code: String)] = []
    var eventTypeSelected : String?
    
    
    @IBOutlet weak var eventName: UITextField!
    
    @IBOutlet weak var eventAbout: UITextView!
    
    @IBOutlet weak var eventDate: UITextField!
    
    @IBOutlet weak var eventTime: UITextField!
    
    @IBOutlet weak var eventType: UITextField!
    
    @IBOutlet weak var eventSubmit: UIButton!
    
    
    @IBAction func createEvent(_ sender: Any) {
        
        createEvent()
        
    }
    
    var pickerView = UIPickerView()
    let picker = UIDatePicker()
    
    var name : String?
    var about : String?
    var date : String?
    var time : String?
    var type : String?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //make corners round
        eventName.layer.cornerRadius = eventName.bounds.width / 20
        eventAbout.layer.cornerRadius = eventAbout.bounds.width / 50
        eventDate.layer.cornerRadius = eventDate.bounds.width / 20
        eventTime.layer.cornerRadius = eventTime.bounds.width / 20
        eventType.layer.cornerRadius = eventType.bounds.width / 20
        eventSubmit.layer.cornerRadius = eventSubmit.bounds.width / 20
        
        eventSubmit.backgroundColor = colorBrandBlue
        //disable autoscroll layout
        self.automaticallyAdjustsScrollViewInsets = false
        
        // Do any additional setup after loading the view.
        
        createDatePicker()
        loadEventTypes()
        pickerView.delegate = self
        pickerView.dataSource = self
        
        eventType.inputView = pickerView
        
        
        
    }
    
    func createDatePicker(){
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        // done button for tool bar
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        
        eventDate.inputAccessoryView = toolbar
        eventDate.inputView = picker
        
        //set date picker to show only date not time
        picker.datePickerMode = .date
        
        
    }
    
    @objc func donePressed(){
        
        //format date
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        let dateString = formatter.string(from: picker.date)
        
        eventDate.text = "\(dateString)"
        self.view.endEditing(true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadEventTypes(){
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/places/event-types")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let eventPosts = try JSONDecoder().decode(userEventType.self, from: data)
                
                //print(userPosts.data)
                //print("it works")
                
                self.eventTypeArray = eventPosts.data
                //iterate through array to get specific values from struct
                
                
              
                for eventArray in self.eventTypeArray{
                    
                    let eventPickerId = eventArray.event_type_id
                    let eventPickerName = eventArray.label
                    
                    self.eventTypeTuple.append((name: eventPickerName! , code: eventPickerId!))
                    
                    DispatchQueue.main.async {
                        self.pickerView.reloadAllComponents()
                    }
                    
                }
       
                
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
    }
    // returns the number of 'columns' to display.
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
        
    }
    
    // returns the # of rows in each component..
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return eventTypeTuple.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return eventTypeTuple[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        eventTypeSelected = eventTypeTuple[row].code
        
        eventType.text = eventTypeTuple[row].name
        
        
        eventType.resignFirstResponder()
        
    }
    
    func createEvent(){
    
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        
        if eventName.text != nil {
            name = eventName.text!
        }else{
            
        }
        
        if eventAbout.text != nil {
            about = eventAbout.text!
        }else{
            
        }
        
        if eventDate.text != nil {
            date = eventDate.text!
        }else{
            
        }
        if eventTime.text != nil {
            time = eventTime.text!
        }else{
            
        }
        if eventType.text != nil {
            type = eventTypeSelected
        }else{
            
        }
        
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/places/add_event")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
         let body = "target_type_id=\(2)&target_id=\(1)&creator_id=\(userId)&event_type_id=\(type!)&name=\(name!)&about=\(about!)&event_date=\(date!)&event_time=\(time!)&is_private=\(0)&place_id=\(1)"
        
        print(body)
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            
                            // get main queue to communicate back to user
                            DispatchQueue.main.async(execute: {
                                let message = "Event successfully added!"
                                
                                self.eventName.text = ""
                                self.eventAbout.text = ""
                                self.eventDate.text = ""
                                self.eventTime.text = ""
                                self.eventType.text = ""
                                
                                appDelegate.infoView(message: message, color: colorBrandBlue)
                            })
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            print(message)
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        print(message)
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
