//
//  CreatePostViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 31/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class CreatePostViewController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //UI objects
    @IBOutlet weak var createPostTextField: UITextView!
    
    @IBOutlet weak var showImageToUpload: UIImageView!
    
    @IBOutlet weak var selectBtn: UIButton!
    
    @IBOutlet weak var postBtn: UIButton!
    
    
     var postActivity:UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var picTaken: UIButton!
    
    var uuid = String()
    var imageSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //make corners round
        createPostTextField.layer.cornerRadius = createPostTextField.bounds.width / 50
        postBtn.layer.cornerRadius = postBtn.bounds.width / 20
        
        //createPostTextField.backgroundColor = colorSmoothGray
        selectBtn.setTitleColor(colorBrandBlue, for: .normal)
        picTaken.setTitleColor(colorBrandBlue, for: .normal)
        
        postBtn.backgroundColor = colorBrandBlue
        
        //disable autoscroll layout
        self.automaticallyAdjustsScrollViewInsets = false
        
        // disable post button until some text is entered
        //postBtn.isEnabled = false
        //postBtn.alpha = 0.4
        
        
       
    }
    
    //clicked select Picture button
    
    @IBAction func select_click(_ sender: Any) {
        
        //calling picker for selecting image
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
        
    }
    
    @IBAction func takePic(_ sender: Any) {
        
        //calling picker for Taking pic

        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        showImageToUpload.image = info[UIImagePickerControllerEditedImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
        
        //cast as true to save image file to server
        imageSelected = true
        
    }
    
    // custom body of HTTP request to upload image file
    @objc func createBodyWithParams(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
               
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        
        // if file is not selected, it will not upload a file to server, because we did not declare a name file
        var filename = ""
        
        if imageSelected == true {
            filename = "post-\(uuid).jpg"
        }
        
        
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")
        
        body.appendString("--\(boundary)--\r\n")
        
        return body as Data
        
    }
    //function sending request to server to upload file
    func uploadImagePost() {
        startAnimating()
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        //variables to be passed to server
        let targetTypeId = "1" as String
        let targetId = userId
        let creatorId = userId
        let postContent = createPostTextField.text as String
        let permissionList = "11111" as String
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/add-media-post")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // param to be passed to php file
        let param = [
            "target_type_id" : targetTypeId,
            "target_id" : targetId,
            "creator_id" : creatorId,
            "content" : postContent,
            "permisson_list" : permissionList
        ]
        
        // body
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        // if picture is selected, compress it by half
        var imageData = Data()
        
        if showImageToUpload.image != nil {
            imageData = UIImageJPEGRepresentation(showImageToUpload.image!, 0.5)!
        }
        
        // Continue Body... body
        request.httpBody = createBodyWithParams(param, filePathKey: "file", imageDataKey: imageData, boundary: boundary)
        
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        //print(message)
                        self.stopAnimating()
                        // if there is some message - post is made
                        if message != nil {
                            
                            // reset UI
                            self.createPostTextField.text = ""
                            self.showImageToUpload.image = nil
                            //self.postBtn.isEnabled = false
                            //self.postBtn.alpha = 0.4
                            self.imageSelected = false
                            let message = "Posted successfully"
                            appDelegate.infoView(message: message, color: colorBrandBlue)
                            // switch to another scene
                            appDelegate.login()
                           
                        }
                        
                    } catch {
                        self.stopAnimating()
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    self.stopAnimating()
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
    }
    //function sending request to server to upload file
    func uploadPost() {
        startAnimating()
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        //variables to be passed to server
        let targetTypeId = "1" as String
        let targetId = userId
        let creatorId = userId
        let postContent = createPostTextField.text as String
        let permissionList = "11111" as String
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/posts/add-post")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "target_type_id=\(targetTypeId)&target_id=\(targetId)&creator_id=\(creatorId)&content=\(postContent)&permisson_list=\(permissionList)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                    
                        self.stopAnimating()
                        // if there is some message - post is made
                        if message != nil {
                            
                            // reset UI
                            self.createPostTextField.text = ""
                            //self.showImageToUpload.image = nil
                            //self.postBtn.isEnabled = false
                            //self.postBtn.alpha = 0.4
                            self.imageSelected = false
                            
                            let message = "Posted successfully"
                            appDelegate.infoView(message: message, color: colorBrandBlue)
                            // switch to another scene
                            appDelegate.login()
                            
                        }
                        
                    } catch {
                        self.stopAnimating()
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    self.stopAnimating()
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
    }
    //tracks when the post is clicked
    @IBAction func post_click(_ sender: Any) {
        //check to see if some text has been entered
        if !createPostTextField.text.isEmpty {
            //send post to server!
            
            if imageSelected == true {
                //if no image has been selected
                 uploadImagePost()
            }else{
                //if an image has been selected
                uploadPost()
            }
  
        }else{
            //do something
        }
        
    }
    
    
    
    /*
    //do certain things if text view changes
    func textViewDidChange(_ textView: UITextView) {
        //number of characters in textview
       let char = textView.text.count
        
        if char > 0 {
            postBtn.isEnabled = true
            postBtn.alpha = 0.4
        }
    }
*/
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //hide keyboard
        self.view.endEditing(false)
        
    }
    
    func startAnimating() {
        postActivity.center = self.view.center
        
        postActivity.hidesWhenStopped = true
        postActivity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(postActivity)
        
        postActivity.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        postActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
    
}
// Creating protocol of appending string to var of type data
extension NSMutableData {
    
    @objc func appendString(_ string : String) {
        
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
        
    }
    
}
