//
//  notifications.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 30/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

struct notificationPost: Decodable {
    
    let status: String
    let message: String
    let data: [notificationData]
    
}

struct notificationData: Decodable {
    let notify_id: String?
    let party_1: String?
    let first_name1: String?
    let last_name1: String?
    let profile_pic1: String?
    let profile_folder_name1: String?
    let profile_pic_filename1: String?
    let party_2: String?
    let first_name2: String?
    let last_name2: String?
    let profile_pic2: String?
    let profile_folder_name2: String?
    let profile_pic_filename2: String?
    let party_3: String?
    let first_name3: String?
    let last_name3: String?
    let date_created: String?
    let notif_str_id: String?
    let str_format: String?
    let possessive: String?
    let type_url: String?
    let is_read: String?
    let active: String?
    let notif_web_url: String?
}
