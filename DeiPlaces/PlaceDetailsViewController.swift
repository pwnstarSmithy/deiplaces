//
//  PlaceDetailsViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 01/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class PlaceDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var placeName : String?
    var placeProfilePicture : String?
    var placeCoverPicture : String?
    var placeAddress : String?
    var placeAbout : String?
    var placePhone = [String]()
    var placeEmail = [String]()
    var placeCategory: String?
    var placeId : String?
    var photo_type : String?
    let targetType = "place"
    
    var postActivity:UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var coverPicPLaces: UIImageView!
    
    @IBOutlet weak var profPicPlaces: UIImageView!
    
    @IBOutlet weak var placeNameTop: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var uuid = String()
    var imageSelected = false
    
    @IBAction func changeCover(_ sender: Any) {
        
        photo_type = "cover"
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Take a Pic", style: .default, handler: { action in
            //call function to load camera
            self.takePic()
            
            print("Take a pic")
        }))
   
        alertController.addAction(UIAlertAction(title: "Choose from photo library", style: .default, handler: { action in
            //call function to load photo library
            self.chooseLibrary()
           print("Choose from library")
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        
        self.present(alertController, animated: true, completion: nil )
        
    }
    
    
    @IBAction func changeProfile(_ sender: Any) {
        photo_type = "profile"
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Take a Pic", style: .default, handler: { action in
            //call function to load camera
            self.takePic()
            print("Take a pic")
        }))
        
        alertController.addAction(UIAlertAction(title: "Choose from photo library", style: .default, handler: { action in
            //call function to load photo library
            self.chooseLibrary()
            print("Choose from library")
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        
        self.present(alertController, animated: true, completion: nil )
        
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if photo_type == "cover"{
            coverPicPLaces.image = info[UIImagePickerControllerEditedImage] as? UIImage
            self.dismiss(animated: true, completion: nil)
            
            //cast as true to save image file to server
            imageSelected = true
            
              uploadPic()
        }else{
            profPicPlaces.image = info[UIImagePickerControllerEditedImage] as? UIImage
            self.dismiss(animated: true, completion: nil)
            
            //cast as true to save image file to server
            imageSelected = true
            
              uploadPic()
        }
        
       
        
    }
    
    // custom body of HTTP request to upload image file
    @objc func createBodyWithParams(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        
        // if file is not selected, it will not upload a file to server, because we did not declare a name file
        var filename = ""
        
        if imageSelected == true {
            filename = "post-\(uuid).jpg"
        }
        
        
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")
        
        body.appendString("--\(boundary)--\r\n")
        
        return body as Data
        
    }
    
    //function to choose from photolibrary
    func chooseLibrary(){
        
        //calling picker for selecting image
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
        
        
    }
    
    func takePic(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
            
        }
      
    }
    
    
    func uploadPic(){
        
     startAnimating()
        userID = UserDefaults.standard.object(forKey: "userId") as? String
        
        let userNum = userID!
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/photos/profile-cover-photo-upload")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // param to be passed to php file
        let param = [
         
            "photo_type" : photo_type!,
            "target_type" : targetType,
            "target_id" : placeId!,
            "creator_id" : userNum
        ]

        
        // body
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        // if picture is selected, compress it by half
        var imageData = Data()
        
        if photo_type == "cover"{
            
            if coverPicPLaces.image != nil {
                imageData = UIImageJPEGRepresentation(coverPicPLaces.image!, 0.5)!
            }
             
        }else {
            if profPicPlaces.image != nil {
                imageData = UIImageJPEGRepresentation(profPicPlaces.image!, 0.5)!
            }
            
        }
        
        
        
        // Continue Body... body
        request.httpBody = createBodyWithParams(param, filePathKey: "file", imageDataKey: imageData, boundary: boundary)
        
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        self.stopAnimating()
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        //print(message)
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            // reset UI
                           
                            self.imageSelected = false
                
                        }
                        
                    } catch {
                        self.stopAnimating()
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                        })
                        return
                        
                    }
                    
                } else {
                    self.stopAnimating()
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
        
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none
        
        
        // dynamic table view cell height
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if placeName != nil {
            
            placeNameTop.text = placeName
            
        }else{
                print("place has no name")
        }
        
        if placeProfilePicture != nil {
            
            //load profile picture from library
            let urlString = "https://deiplaces.com/uploads/profile-picture/image_500/"+(placeProfilePicture)!
            let profileURL = URL(string: urlString)
            
            profPicPlaces?.downloadedFrom(url: profileURL!)
            
            
        }else{
            
        }
        
        if placeCoverPicture != nil {
            //load profile picture from library
            let urlString = "https://deiplaces.com/uploads/cover-picture/image_500/"+(placeCoverPicture)!
            let profileURL = URL(string: urlString)
            
            coverPicPLaces?.downloadedFrom(url: profileURL!)
            
            
        }else{
            print("no cover Pic")
        }
        
        // Do any additional setup after loading the view.
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if section == 0{
            return 1
        }else{
            return 1
        }
        
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "statusCellPlace", for: indexPath) as! placeStatusTableViewCell
            
            profilePicture = UserDefaults.standard.object(forKey: "userPic") as? String
            
            if profilePicture != nil {
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/"+(profilePicture)!
                let profileURL = URL(string: urlString)
                
                cell.placesProfilePicTop?.downloadedFrom(url: profileURL!)
                
            }else{
                
                print("you have no profile picture set")
                
            }
            return cell
        }else{
             let cell = tableView.dequeueReusableCell(withIdentifier: "PostCellPlace", for: indexPath) as! placesPostsTableViewCell
            if placeName != nil {
            cell.placeName.text = placeName
            }else{
                print("no name")
            }
            if placeAddress != nil {
                print(placeAddress!)
            cell.placeAddress.text = placeAddress
            }else{
                print("no Address")
            }
            
           if placeCategory != nil {
                cell.placesCategory.text = placeCategory
            }else{
                
                print("No category")
                
            }
            
            if placePhone.isEmpty {
                
               print("Place has no phone number!")
               
            }else{
                //iterate through array
                for phonePhone in placePhone {
                    print(phonePhone)
                    cell.placesPhone.text = phonePhone
                }
                
            }
            
            
            if placeEmail.isEmpty {
                
                print("Place has no Email Address")
                
            }else{
                
                for emailEmail in placeEmail{
                    print(emailEmail)
                    cell.placesEmail.text = emailEmail
                    
                }
                
            }
            
            if placeAbout != nil {
            cell.placeAbout.text = placeAbout
            }else{
                print("no information")
            }
            
            return cell
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    func startAnimating() {
        postActivity.center = self.view.center
        
        postActivity.hidesWhenStopped = true
        postActivity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(postActivity)
        
        postActivity.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        postActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
}
