//
//  NoImageProfilePostTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 16/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class NoImageProfilePostTableViewCell: UITableViewCell {

    
    @IBOutlet weak var noImageProfPic: UIImageView!
    
    @IBOutlet weak var noImageUsername: UILabel!
    
    @IBOutlet weak var noImageTime: UILabel!
    
    @IBOutlet weak var noimageCaption: UILabel!
    
    @IBOutlet weak var noImageStats: UILabel!
    
    @IBOutlet weak var postID: UILabel!
    
    @IBAction func moreTapped(_ sender: Any) {
        
        
    }
    
    
}
