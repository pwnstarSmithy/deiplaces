//
//  hotelsViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 24/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class hotelsViewController: UIViewController{
    
    var destinationKeyword : String?
    
    var destinationBoundingBox = [String]()
    
    var locationDetails : location?
    
    var cleanedBoundingBox = [String]()
    
    var cleanBox1 : String?
    var cleanBox2 : String?
    var cleanBox3 : String?
    var cleanBox4 : String?
    
    var hotelsArray = [hotelResults]()
    
    @IBOutlet weak var startText: UITextField!
    
    @IBOutlet weak var endText: UITextField!
    
    @IBOutlet weak var adults: UITextField!
    
    @IBOutlet weak var child: UITextField!
    
    @IBOutlet weak var rooms: UITextField!
    
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBAction func searchPressed(_ sender: Any) {
        
        
        
    }
    
    let picker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        destinationKeyword = locationDetails?.display_name
        locationLabel.text = destinationKeyword
        
        destinationBoundingBox = (locationDetails?.boundingbox!)!
        
      
        
        for cleanBox in destinationBoundingBox {
            
            let cleaning = cleanBox.replacingOccurrences(of: "\"", with: "")
            
            self.cleanedBoundingBox.append(cleaning)
            
            
            
        }
//        print(cleanedBoundingBox)
        
        let box1 = cleanedBoundingBox[0]
        let box2 = cleanedBoundingBox[1]
        let box3 = cleanedBoundingBox[2]
        let box4 = cleanedBoundingBox[3]
        
        cleanBox1 = box1.replacingOccurrences(of: "\"", with: "")
        cleanBox2 = box2.replacingOccurrences(of: "\"", with: "")
        cleanBox3 = box3.replacingOccurrences(of: "\"", with: "")
        cleanBox4 = box4.replacingOccurrences(of: "\"", with: "")
        
        print(cleanBox1!)
        
     
        
//        print(destinationBoundingBox)
        createDatePicker()
        
        // Do any additional setup after loading the view.
        //search for hotels based on location
        
        DispatchQueue.main.async(execute: {
            
            self.getHotel()
        })
    }
    
    func getHotel() {

        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/places/find_hotel?bbox=\(cleanBox1!),\(cleanBox2!),\(cleanBox3!),\(cleanBox4!)")else{return}
        
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let hotelInfo = try JSONDecoder().decode(hotels.self, from: data)
        
                self.hotelsArray =  hotelInfo.data
                
                print("Hotel Assigned")
                print(self.hotelsArray)
                
            }catch{
                print(error)
                
            }
            
        }
    
        task.resume()
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? HotelsTableViewController {
            
            if hotelsArray.isEmpty{
                
                  let alertController = UIAlertController(title: "No Hotels", message: "Your search didn't return any hotels in this area.", preferredStyle: .alert)
                
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self.present(alertController, animated: true, completion: nil )
                
                
                
            }else{
                DispatchQueue.main.async(execute: {
                    destination.hotelsArray = self.hotelsArray
                     destination.tableView.reloadData()
                })
            }
            
         
            
            
        }
    }
    
    func createDatePicker(){
        
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        // done button for tool bar
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        
        startText.inputAccessoryView = toolbar
        startText.inputView = picker
        
        //set date picker to show only date not time
        picker.datePickerMode = .date
        
        
    }
    
    @objc func donePressed(){
        
        //format date
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        let dateString = formatter.string(from: picker.date)
        
        startText.text = "\(dateString)"
        self.view.endEditing(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
