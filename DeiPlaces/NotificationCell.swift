//
//  notificationCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 28/03/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class NotificationCell : UITableViewCell
{

    @IBOutlet weak var notificationProfilePic: UIImageView!
    
  
    
    @IBOutlet weak var notificationName: UILabel!
    
    @IBOutlet weak var notificationLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setRoundedView(roundedView: notificationProfilePic)
    }
    
    func setRoundedView (roundedView:UIView) {
        let saveCenter = roundedView.center
        let newFrame:CGRect = CGRect(origin: CGPoint(x: roundedView.frame.origin.x,y :roundedView.frame.origin.y), size: CGSize(width: roundedView.frame.size.width, height: roundedView.frame.size.height))
        roundedView.layer.cornerRadius = roundedView.frame.height/2
        roundedView.frame = newFrame;
        roundedView.center = saveCenter
        roundedView.clipsToBounds = true
    }
}



