//
//  eventsCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 01/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

protocol eventsDelegate {
   func didTapMore(id: String)
}

class EventsCell : UITableViewCell
{
    @IBOutlet weak var eventsImage: UIImageView!
    
    @IBOutlet weak var eventsName: UILabel!
    
    @IBOutlet weak var eventsOwner: UILabel!
    
    @IBOutlet weak var eventsStartdate: UILabel!
    
    @IBOutlet weak var eventsEnddate: UILabel!
    
    @IBOutlet weak var postID: UILabel!
    
    var delegate: eventsDelegate?
    
    @IBOutlet weak var eventsInfo: UILabel!
    
    @IBAction func moreTapped(_ sender: Any) {
        
        delegate?.didTapMore(id: postID.text!)
    }
    
}
