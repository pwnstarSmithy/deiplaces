//
//  SearchEventsTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 25/05/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class SearchEventsTableViewCell: UITableViewCell {

    @IBOutlet weak var eventsLabel: UILabel!
    
    @IBOutlet weak var eventsImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
