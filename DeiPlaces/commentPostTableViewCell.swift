//
//  commentPostTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 15/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

protocol commentPostCellDelegate {
    func didTapMore(id: String)
    func didLike(id: String)
}

class commentPostTableViewCell: UITableViewCell {

    @IBOutlet weak var timeAgoLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var postStatsLabel: UILabel!

    @IBOutlet weak var postID: UILabel!
    
    var delegate: commentPostCellDelegate?
    
    @IBAction func likePressed(_ sender: Any) {
        
        delegate?.didLike(id: postID.text!)
    }
    
    
    @IBAction func moreTapped(_ sender: Any) {
        
        delegate?.didTapMore(id: postID.text!)
    }
    
    
    
}
