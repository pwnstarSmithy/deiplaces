//
//  FriendsListTableViewCell.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 18/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

protocol FriendsListDelegate {
    func didUnfollow(id: String)
    func didUnfriend(id: String)
}

class FriendsListTableViewCell: UITableViewCell {

    @IBOutlet weak var friendsListProfPic: UIImageView!
    
    @IBOutlet weak var friendName: UILabel!
    
    @IBOutlet weak var receipientID: UILabel!
    
    var delegate: FriendsListDelegate?
    
    @IBAction func blockFriend(_ sender: Any) {
        
        delegate?.didUnfollow(id: receipientID.text!)
        
    }
    
    @IBAction func unfriend(_ sender: Any) {
        
        delegate?.didUnfriend(id: receipientID.text!)
    }
    

}
