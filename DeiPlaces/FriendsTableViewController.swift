//
//  FriendsTableViewController.swift
//  DeiPlaces
//
//  Created by pwnstarSmithy on 18/04/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class FriendsTableViewController: UITableViewController, FriendRequestDelegate, FriendsListDelegate {
    
    var postActivity:UIActivityIndicatorView = UIActivityIndicatorView()
    
    var friendsArray = [friendsData]()
    
    var requestsArray = [requestData]()
    var postIdentification : String?
    
    var followerId : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async(execute: {
            self.getFriend()
            self.getRequests()
        })
        
        tableView.separatorStyle = .none
        
        // dynamic table view cell height
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {
            self.startAnimating()
            self.getFriend()
            self.getRequests()
            self.stopAnimating()
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return requestsArray.count
            
        }else{
            return friendsArray.count
            
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "FRIEND REQUESTS"
        }else {
            return "YOUR FRIENDS"
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
               let cell = tableView.dequeueReusableCell(withIdentifier: "friendRequests", for: indexPath) as! FriendRequestTableViewCell
            
            
            if requestsArray[indexPath.row].profile_pic_filename != nil {
                profilePicture = requestsArray[indexPath.row].profile_pic_filename
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/"+(profilePicture)!
                let profileURL = URL(string: urlString)
                
                cell.FriendRequestProfPic?.downloadedFrom(url: profileURL!)
             
            }else{
                
                print("you have no profile picture set")
                
            }
            cell.friendRequestName.text = requestsArray[indexPath.row].fullname
            //cell.friendId.text = requestsArray[indexPath.row].user_id
            cell.receipientID.text = requestsArray[indexPath.row].user_id
             cell.delegate = self
            return cell
            
        }else{
               let cell = tableView.dequeueReusableCell(withIdentifier: "friendsList", for: indexPath)  as! FriendsListTableViewCell
            
            
            if friendsArray[indexPath.row].profile_pic_filename != nil {
                profilePicture = friendsArray[indexPath.row].profile_pic_filename
                //load profile picture from library
                let urlString = "https://deiplaces.com/uploads/profile-picture/"+(profilePicture)!
                let profileURL = URL(string: urlString)
                
                cell.friendsListProfPic?.downloadedFrom(url: profileURL!)
                
            }else{
                
                print("you have no profile picture set")
                
            }
            cell.friendName.text = friendsArray[indexPath.row].fullname
            
            cell.receipientID.text = friendsArray[indexPath.row].user_id
            cell.delegate = self
            return cell
        }
        
    }
    
    //function to get user information
    func getFriend() {
        
        userID = UserDefaults.standard.object(forKey: "userId") as? String
        
        let userNum = userID!
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/user/single-user-friends?user_id=\(userNum)")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userInfo = try JSONDecoder().decode(friendsList.self, from: data)
                
                
                let usersArray = userInfo.data
                //iterate through array to get specific values from struct
                self.friendsArray = usersArray.friends!
         
                
                for friendlyArray in self.friendsArray{
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                        
                        print(friendlyArray)
                        
                    })
                    }
                
    
                
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
        
    }
    func  getRequests() {
        
        userID = UserDefaults.standard.object(forKey: "userId") as? String
        
        let userNum = userID!
        
        guard let url = URL(string: "https://deiplaces.com/api/index.php/user/single_user_follows?user_id=\(userNum)&type=followers")else{return}
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {return}
            
            do{
                let userInfo = try JSONDecoder().decode(requestList.self, from: data)
                
                
                let usersArray = userInfo.data
                //iterate through array to get specific values from struct
                if usersArray.followers != nil{
                    self.requestsArray = usersArray.followers!
                    
                }else{
                    print("you don't have any followers")
                }
                
                for requestlyArray in self.requestsArray{
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                        
                        print(requestlyArray)
                        
                    })
                }
                
                
            }catch{
                print(error)
                
            }
            
        }
        task.resume()
        
        
        
    }
    
    func didAccept(id: String){
        
        postIdentification = id
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        let sendAction = "confirm"
        
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/user/friend-request")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "sender_id=\(userId)&recipient_id=\(postIdentification!)&action=\(sendAction)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        
                      
                        // if there is some message - post is made
                        if message != nil {
                            
                            let messageCustom = message
                            appDelegate.infoView(message: messageCustom as! String, color: colorBrandBlue)
                            
                            
                            DispatchQueue.main.async(execute: {
                              self.getRequests()
                            let requestSection = self.requestsArray
                          
                            var indexPathsToReload = [IndexPath]()
                            for index in requestSection.indices{
                                
                                let indexPath = IndexPath(row: index, section: 0)
                                indexPathsToReload.append(indexPath)
                                
                            }
                            self.tableView.reloadRows(at: indexPathsToReload, with: .right)
                           })
                            
                        }else{
                            
                            print("You have no friends")
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                            
                            print(error)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
    }
    
    func didDecline(id: String){
        
        
        postIdentification = id
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        let sendAction = "reject"
        
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/user/friend-request")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "sender_id=\(userId)&recipient_id=\(postIdentification!)&action=\(sendAction)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            let messageCustom = message
                            appDelegate.infoView(message: messageCustom as! String, color: colorBrandBlue)
                            
                            
                            DispatchQueue.main.async(execute: {
                                self.getRequests()
                                let requestSection = self.requestsArray
                                
                                var indexPathsToReload = [IndexPath]()
                                for index in requestSection.indices{
                                    
                                    let indexPath = IndexPath(row: index, section: 0)
                                    indexPathsToReload.append(indexPath)
                                    
                                }
                                self.tableView.reloadRows(at: indexPathsToReload, with: .right)
                            })
                            
                        }else{
                            
                            print("You have no friends")
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                            
                            print(error)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
            })
            
            }.resume()
        
    }
    
    func didUnfollow(id: String){
        
        let requesterId = id
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        let sendAction = "disconnect"
        
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/user/friend-request")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "sender_id=\(userId)&recipient_id=\(requesterId)&action=\(sendAction)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            let messageCustom = "Successfully blocked user!"
                            appDelegate.infoView(message: messageCustom, color: colorBrandBlue)
                            //print(messageCustom)
                            
                            DispatchQueue.main.async(execute: {
                                self.getFriend()
                                let friendSection = self.friendsArray
                                
                                var indexPathsToReload = [IndexPath]()
                                for index in friendSection.indices{
                                    
                                    let indexPath = IndexPath(row: index, section: 1)
                                    indexPathsToReload.append(indexPath)
                                    
                                }
                                self.tableView.reloadRows(at: indexPathsToReload, with: .right)
                            })
                            
                        }else{
                            
                            self.tableView.reloadData()
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                            
                            print(error)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
        
    }
    func didUnfriend(id: String){
        let requesterId = id
        //assign userid from saved information
        let userId = UserDefaults.standard.object(forKey: "userId") as! String
        let sendAction = "disconnect"
        
        
        // url path to php file
        let url = URL(string: "https://deiplaces.com/api/index.php/user/friend-request")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let body = "sender_id=\(userId)&recipient_id=\(requesterId)&action=\(sendAction)"
        
        request.httpBody = body.data(using: String.Encoding.utf8)
        // launch session
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            // get main queu to communicate back to user
            DispatchQueue.main.async(execute: {
                
                
                if error == nil {
                    
                    do {
                        
                        // json containes $returnArray from php
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        
                        // declare new var to store json inf
                        guard let parseJSON = json else {
                            print("Error while parsing")
                            return
                        }
                        
                        // get message from $returnArray["message"]
                        let message = parseJSON["message"]
                        
                        
                        // if there is some message - post is made
                        if message != nil {
                            
                            let messageCustom = message
                            appDelegate.infoView(message: messageCustom as! String, color: colorBrandBlue)
                            print(messageCustom!)
                            
                            DispatchQueue.main.async(execute: {
                                self.getFriend()
                                let friendSection = self.friendsArray
                                
                                var indexPathsToReload = [IndexPath]()
                                for index in friendSection.indices{
                                    
                                    let indexPath = IndexPath(row: index, section: 1)
                                    indexPathsToReload.append(indexPath)
                                    
                                }
                                self.tableView.reloadRows(at: indexPathsToReload, with: .right)
                            })
                            
                        }else{
                            
                            self.tableView.reloadData()
                            
                        }
                        
                    } catch {
                        
                        // get main queue to communicate back to user
                        DispatchQueue.main.async(execute: {
                            let message = "\(error)"
                            appDelegate.infoView(message: message, color: colorSmoothRed)
                            
                            print(error)
                        })
                        return
                        
                    }
                    
                } else {
                    
                    // get main queue to communicate back to user
                    DispatchQueue.main.async(execute: {
                        let message = error!.localizedDescription
                        appDelegate.infoView(message: message, color: colorSmoothRed)
                    })
                    return
                    
                }
                
                
            })
            
            }.resume()
        
    }
    
    func unfollowUser(){
        
        
        
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func startAnimating() {
        postActivity.center = self.view.center
        
        postActivity.hidesWhenStopped = true
        postActivity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        view.addSubview(postActivity)
        
        postActivity.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func stopAnimating(){
        
        postActivity.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        
    }
}
